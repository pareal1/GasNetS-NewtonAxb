#!/usr/bin/env python
# vim: set fileencoding=utf-8 fileformat=unix :
# -*- coding: utf-8 -*-
# vim: set ts=8 et sw=4 sts=4 sta :

from math import *
import numpy as np
import scipy as sp

print("""
Exercício:
Considere um edifício composto por 4 pisos com 2 fracções por piso. Cada
fracção contém um esquentador de 11 L/min e um fogão pequeno com forno.
O gás natural que alimenta os pontos de consumo tem uma densidade de 0,65
e um PCI de 37,91MJ/m3(T=0°C). Calcule:

1. O caudal de projecto a considerar para o edifício para uma temperatura
de referência de T=25°C (assuma Z≈1).

2. O comprimento das condutas, L, e o comprimento equivalente, L.

3. Verifique a influência de considerar um valor de Z realista no cálculo
que efectuou do caudal de projecto. Pode fazer uso da equação aproximada de
Garb (diapositivo 5) e do método de Papp (diapositivo 11).
""")

Runiv = 8314.41  # J/kg/K
Mar = 28.9625  # kg/kmol

## Input

Npisos = 4
Nfraccoes = 2

d = 0.65

PCIn = 37.91e3  # kJ/m3 (n)
Tn = 273.15

T0 = 288.15
P0 = 101325.

T = 25 + 273.15



print("""
#######
##
## Resolução
##
#######
""")

print("\n1.")

## esquentador de 11 L/min + fogão pequeno com forno
Qfogo = 23. + 9.  # kW



N = Npisos * Nfraccoes

if N == 1:
    Csim = 1
elif N == 2:
    Csim = .7
elif N == 3:
    Csim = .6
elif N == 4:
    Csim = .55
elif 5 <= N <= 7:
    Csim = .5
elif 8 <= N <= 10:
    Csim = .45
elif N > 10:
    Csim = .4

Q = N * Qfogo * Csim

print("Qfogo = %g kW" % Qfogo)
print("Nfogos = ", N, "   Csim = ", Csim)
print("Q     = %g kW" % Q)

M = d * Mar
R = Runiv / M

Z0 = 1.
Zn = 1.
Z = 1.

P = P0
Pn = P0

PCI0 = PCIn * Zn/Z0 * Tn/T0 * P0/Pn  # kJ/m3
PCI = PCIn * Zn/Z * Tn/T * P/Pn  # kJ/m3

PCIm = PCIn * Zn*R*Tn/Pn  # kJ/kg
#PCIm = PCI0 * Z0*R*T0/P0  # kJ/kg
#PCIm = PCI * Z*R*T/P  # kJ/kg

## Podemos usar o caudal mássico
# m = Q / PCIm  # kg/s
# V0 = m * Z0*R*T0/P0  # m3/s a T0
# V = m * Z*R*T/P  # m3/s a T

## Ou podemos juntar tudo na mesma expressão e trabalhar com V0 apenas
# V0 = Z0*T0/P0 * Q / PCIn * Pn/Zn/Tn  # forma 1
# V0 = Z0*T0/P0 * Q / PCI * P/Z/T      # forma 2
V0 = Q / PCI0

## Podemos fazer o mesmo para V em vez de V0
# V = Z*T/P * Q / PCIn * Pn/Zn/Tn  # forma 1
# V = Z*T/P * Q / PCI0 * P0/Z0/T0  # forma 2
V = Q / PCI

print("PCI0 = %g kJ/m3" % PCI0)
print("V0   = %g m3/s" % V0)
print("PCI  = %g kJ/m3" % PCI)
print("V    = %g m3/s" % V)


print("\n2. O comprimento das condutas, L, e o comprimento equivalente, L")

Lmin = 10.  # m
Hpiso = 3.  # m

L = Npisos * 3 + Lmin
Leq = 1.2 * L

print("L   = %g m" % L)
print("Leq = %g m" % Leq)




print("\n3 Verifique a influência de considerar um valor de Z realista no cálculo")

def pontoCriticoGarb (dgas):
    """
    Method to estimate the pseudocritical conditions (Tpc, Ppc) through
    the relative density of the gas.
    Garb (1978) "Moving Averages and Gas Deviation Factor", Petrol Eng Int 50:56-62
    In: "Property Evaluation With Handheld Computers" 
    dgas = RHOgas / RHOar
    """
    Tpc = (167 + 316.67*dgas) * 5/9.  # K
    Ppc = (702.5 - 50*dgas) * 6894.757293168361  # Pa
    return Tpc, Ppc


def ZPapp (Tr, Pr):
    """
    Explicit method to compute Standing & Katz Z compressibility factor.
    Ref: Takacs (1989) "Comparing Methods for Calculating z Factor"
         Oil & Gas J, May 15, pp 43-46
    """
    x = Pr / Tr**2
    a = 0.1219 * Tr**0.638
    b = Tr - 7.76 + 14.75/Tr
    c = 0.3*x + 0.441*x**2
    return 1 + a*(x - b)*(1 - np.exp(-c))


Tpc, Ppc = pontoCriticoGarb(d)

print("Tpc = %g K" % Tpc)
print("Ppc = %g Pa" % Ppc)

Z = ZPapp (T/Tpc, P/Ppc)
Z0 = ZPapp (T0/Tpc, P0/Ppc)
Zn = ZPapp (Tn/Tpc, Pn/Ppc)

print("Z  = %g" % Z)
print("Z0 = %g" % Z0)
print("Zn = %g" % Zn)



PCI0 = PCIn * Zn/Z0 * Tn/T0 * P0/Pn  # kJ/m3
PCI = PCIn * Zn/Z * Tn/T * P/Pn  # kJ/m3

PCIm = PCIn * Zn*R*Tn/Pn  # kJ/kg

## Podemos usar o caudal mássico
# m = Q / PCIm  # kg/s
# V0 = m * Z0*R*T0/P0  # m3/s a T0
# V = m * Z*R*T/P  # m3/s a T

## Ou podemos juntar tudo na mesma expressão e trabalhar com V0 apenas
# V0 = Z0*T0/P0 * Q / PCIn * Pn/Zn/Tn  # forma 1
# V0 = Z0*T0/P0 * Q / PCI * P/Z/T      # forma 2
V0 = Q / PCI0

## Podemos fazer o mesmo para V em vez de V0
# V = Z*T/P * Q / PCIn * Pn/Zn/Tn  # forma 1
# V = Z*T/P * Q / PCI0 * P0/Z0/T0  # forma 2
V = Q / PCI

print("PCI0 = %g kJ/m3" % PCI0)
print("V0   = %g m3/s" % V0)
print("PCI  = %g kJ/m3" % PCI)
print("V    = %g m3/s" % V)


