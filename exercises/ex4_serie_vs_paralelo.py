#!/usr/bin/env python3
# vim: set fileencoding=utf-8 fileformat=unix :
# -*- coding: utf-8 -*-
# vim: set ts=8 et sw=4 sts=4 sta :

from math import *
import numpy as np
import scipy as sp
import matplotlib as mpl
import matplotlib.pyplot as plt


print("""
Considere uma rede de gás natural (d=0.65) que transporta 5000 m^3/h
(condições padrão 15 degC, 1 atm) através de um percurso de 120 km,
sendo a pressão a montante de 20 bar. Assuma Zm = 1, Tm = 15 degC
e mu = 1.0757e-5 Pa·s.

Calcule a pressão no consumo sob as seguintes situações:

1. Uma única tubagem PEø200 (D = 177.3 mm, h = 11.4 mm)

2. Duas tubagens em paralelo:
   · PEø200 (Da = 177.3 mm, ha = 11.4 mm)
   · PEø110 (Db =  90.0 mm, hb = 10.0 mm)

Para todas as situações verifique se as tubagens sao adequadas
considerando uma tensão de cedência à tração de 29 MPa
e que a localização se enquadra na categoria 1 (F = 0.72).

Utilize a eq. da "IGT Distribuition" nos cálculos que efectuar:

    Pi^2 - Pj^2 = Cij * V0ij^1.8
    
    com:  Cij = 119.8503 * Lij * Tm * mu^0.2 * d^0.8 / Dij^4.8

3. Compare a solução obtida no ponto 1 (tubagem única) para os
seguintes modelos das perdas de carga em linha:

    Panhandle A, Panhandle B
    Colebrook-White
    Mueller
    Fritzsche
    Renouard
""")

Runiv = 8314.41  # J/kg/K
Mar = 28.9625  # kg/kmol

d = 0.65

T0 = 288.15  # K
P0 = 101325.  # Pa


V0e = 5000 / 3600.  # m^3/s, entrada
Pe = 20e5  # Pa
L = 120e3  # m

Zm = 1.
Tm = 15 + 273.15  # K
mu = 1.0757e-5  # Pa·s

Dexta = 200e-3  # m
Da = 177.3e-3  # m
ha = 11.4e-3  # m

Dextb = 110e-2  # m
Db = 90.0e-3  # m
hb = 10.0e-3  # m

Tced = 29e6  # Pa
Fced = 0.72


#Da = 0.5
#Db = 0.4
#Pe = 1000 * 6894.7572931783
#V0e = 200000. / 3600
#L = 322e3


## Panhandle A
#n = 1.854
#fCij = lambda D, L, Tm, d, mu, Zm=1: 10.3895 * L * Zm * Tm * d**0.854 / D**4.854

## IGT
n = 1.8
fCij = lambda D, L, Tm, d, mu: 119.8503 * L * Tm * mu**0.2 * d**0.8 / D**4.8

## Mueller
#n = 1.74
#fCij = lambda D, L, Tm, d, mu: 221.8387 * L * Tm * mu**0.26 * d**0.74 / D**4.74

## Renouard
#n = 1.82
#fCij = lambda D, L, Tm, d, mu: 4810. * L * d / D**4.82


print("""
#######
##
## Resolução
##
#######
""")

## espessura das condutas => Plim

Pceda = 2 * Tced * ha / Dexta * Fced
Pcedb = 2 * Tced * hb / Dextb * Fced

print("Pced,a = %g bar" % (Pceda / 1e5))
print("Pced,b = %g bar" % (Pcedb / 1e5))


print("\n" + 32*"-" + "\nsituação 1: única tubagem")

M = d * Mar
R = Runiv / M

Ca = fCij (Da, L, Tm, d, mu)
Cb = fCij (Db, L, Tm, d, mu)


## Pe**2 - Ps**2 = Ca * V0e**n  =>  Ps = sqrt(Pe**2 - Ca * V0e**n)

Ps = sqrt(Pe**2 - Ca * V0e**n)

print("Ces,a = %g Pa^2 / (m^3/s)^n" % Ca)
print("Pe = %g bar" % (Pe/1e5))
print("Ps = %g bar" % (Ps/1e5))

if max(Pe, Ps) >= Pceda:
    print("existe hipótese de cedência!")
else:
    print("pressões abaixo do limite de cedência")


print("\n" + 32*"-" + "\nsituação 2: duas tubagens em paralelo")

print("""
fracção de caudal que segue pela tubagem a
Fa = V0a / V0e
fracção de caudal que segue pela tubagem b
Fb = V0b / V0e = 1 - Fa

dP2es = Pe**2 - Ps**2

dP2es  =  Ca * V0a**n  =  Cb * V0b**n
<=>  Ca * (V0a/V0e)**n  =  Cb * (V0b/V0e)**n
<=>  Ca * Fa**n  =  Cb * (1-Fa)**n

 =>  Fa = 1./(1. + (Ca/Cb)**(1./n))
""")

Fa = 1./(1. + (Ca/Cb)**(1./n))

V0a = Fa * V0e
V0b = (1-Fa) * V0e

Ps = sqrt(Pe**2 - Ca * V0a**n)
#Ps = sqrt(Pe**2 - Cb * V0b**n)  # tem de ser igual


print("Ces,a = %g Pa^2 / (m^3/s)^n" % Ca)
print("Ces,b = %g Pa^2 / (m^3/s)^n" % Cb)
print("Fa = %g %%" % (Fa*100))
print("Fb = %g %%" % ((1-Fa)*100))
print("V0a = %g m^3/h" % (V0a * 3600))
print("V0b = %g m^3/h" % (V0b * 3600))
print("Pe = %g bar" % (Pe/1e5))
print("Ps = %g bar" % (Ps/1e5))

if max(Pe, Ps) >= Pceda:
    print("tubagem a: existe hipótese de cedência!")
else:
    print("tubagem a: pressões abaixo do limite de cedência")

if max(Pe, Ps) >= Pcedb:
    print("tubagem b: existe hipótese de cedência!")
else:
    print("tubagem b: pressões abaixo do limite de cedência")

print("\n" + 32*"-" + """
alternativamente:

definir Cequivalente tal que:  dP2es  = Ceq * V0e**n

V0e = V0a + V0b  <=>
<=>  (dP2es/Ceq)**(1/n)  =  (dP2es/Ca)**(1/n) + (dP2es/Cb)**(1/n)
<=>  1/Ceq**(1/n)  =  1/Ca**(1/n) + Cb**(1/n)

=>  Ceq = 1./(1/Ca**(1/n) + 1/Cb**(1/n))**n
""")

Ceq = 1./(1/Ca**(1/n) + 1/Cb**(1/n))**n
Ps = sqrt(Pe**2 - Ceq * V0e**n)

print("Ceq = %g Pa^2 / (m^3/s)^n" % Ceq)
print("Ps = %g bar" % (Ps/1e5))






print("""\n\n\n
3. Compare a solução obtida no ponto 1 (tubagem única) para os
seguintes modelos das perdas de carga em linha:

    Mueller
    Fritzsche
    Renouard
    Panhandle A, Panhandle B
    Colebrook-White (ou AGA para tubos lisos)
""")

## IGT
n = 1.8
fCij = lambda D, L, Tm, d, mu: 119.8503 * L * Tm * mu**0.2 * d**0.8 / D**4.8
Ca = fCij (Da, L, Tm, d, mu)
Ps = sqrt(Pe**2 - Ca * V0e**n)
print("IGT:             Ps = %g bar" % (Ps/1e5))

## Mueller
n = 1.74
fCij = lambda D, L, Tm, d, mu: 221.8387 * L * Tm * mu**0.26 * d**0.74 / D**4.74
Ca = fCij (Da, L, Tm, d, mu)
Ps = sqrt(Pe**2 - Ca * V0e**n)
print("Mueller:         Ps = %g bar" % (Ps/1e5))

## Fritzsche
n = 1.858
fCij = lambda D, L, Tm, d, mu: 11.5391 * L * Tm * d**0.858 / D**5
Ca = fCij (Da, L, Tm, d, mu)
Ps = sqrt(Pe**2 - Ca * V0e**n)
print("Fritzsche:       Ps = %g bar" % (Ps/1e5))

## Renouard
n = 1.82
fCij = lambda D, L, Tm, d, mu: 4810. * L * d / D**4.82
Ca = fCij (Da, L, Tm, d, mu)
Ps = sqrt(Pe**2 - Ca * V0e**n)
print("Renouard:        Ps = %g bar" % (Ps/1e5))

## Panhandle A
n = 1.854
fCij = lambda D, L, Tm, d, mu, Zm=1: 10.3895 * L * Zm * Tm * d**0.854 / D**4.854
Ca = fCij (Da, L, Tm, d, mu)
Ps = sqrt(Pe**2 - Ca * V0e**n)
print("Panhandle A:     Ps = %g bar" % (Ps/1e5))

## Panhandle B
n = 1.9609
fCij = lambda D, L, Tm, d, mu, Zm=1: 6.4498 * L * Zm * Tm * d**0.9608 / D**4.9608
Ca = fCij (Da, L, Tm, d, mu)
Ps = sqrt(Pe**2 - Ca * V0e**n)
print("Panhandle A:     Ps = %g bar" % (Ps/1e5))

## Colebrook-White
## Re = 4 * RHOm * V / mu / pi / D
## RHOm * V = RHO0 * V0
Z0 = Zm
RHO0 = P0/Z0/R/T0
Re = 4 * RHO0 * V0e / mu / pi / Da
E = 0  # tubo liso
## cálculo de f
fRootFun = lambda f: 1./sqrt(abs(f)) + 2*log10(E/Da/3.7 + 2.825/Re/sqrt(abs(f)))
from scipy.optimize import newton
f = newton(fRootFun, 2e-2)
## cálculo do coeficiente Cij
n = 2
Ca = 698.825 * L * Zm * Tm * d * f / Da**5
Ps = sqrt(Pe**2 - Ca * V0e**n)
print("Colebrook-White: Ps = %g bar" % (Ps/1e5))










