#!/usr/bin/env python3
# vim: set fileencoding=utf-8 fileformat=unix :
# -*- coding: utf-8 -*-
# vim: set ts=8 et sw=4 sts=4 sta :

from math import *
import numpy as np
import scipy as sp
import matplotlib as mpl
import matplotlib.pyplot as plt


print("""
Considere uma rede de gás natural (d=0.65) que transporta 8 m^3/s
(condições padrão 15 degC, 1 atm) através dois percursos com 50 km
cada um. A pressão a jusante é de 3 bar e considere um diâmetro de
0.2 m para todas as tubagens. Utilizando a equação do "IGT",

    Pi^2 - Pj^2 = Cij * V0ij^1.8
    
    com:  Cij = 119.8503 * Lij * Tm * mu^0.2 * d^0.8 / Dij^4.8

e assumindo Zm = 1 e mu = 1.0757e-5 Pa·s, analise as seguintes opções:

1. Para uma situação com Tm = 300 K, avalie a hipótese de ter:
   a)  P1 ---- P2 ---- P3   (associação em série)
   b)  P1 ==== P2 ---- P3   (1-2 paralelo, 2-3 série)
   c)  P1 ---- P2 ==== P3   (1-2 série, 2-3 paralelo)


2. Repita o cálculo do ponto 1 mas considerando a seguinte distribuição
   de temperaturas:  T1 = 30 degC,  T2 = 25 degC,  T3 = 20 degC
""")

Runiv = 8314.41  # J/kg/K
Mar = 28.9625  # kg/kmol

d = 0.65

T0 = 288.15  # K
P0 = 101325.  # Pa


V0 = 8.  # m^3/s, entrada
P3 = 3e5  # Pa
L = 50e3  # m
D = 0.2  # m

mu = 1.0757e-5  # Pa·s


## IGT
n = 1.8
fCij = lambda D, L, Tm, d, mu: 119.8503 * L * Tm * mu**0.2 * d**0.8 / D**4.8


print("""
#######
##
## Resolução 1
##
#######
""")

Tm = 300.

C = fCij (D, L, Tm, d, mu)
Ceq = C**2/(2*C**(1/n))**n


P2 = sqrt(P3**2 + C*V0**n)
P1 = sqrt(P2**2 + C*V0**n)
print("\na)")
print("P1 = %g bar" % (P1/1e5))
print("P2 = %g bar,  P1-P2 = %g bar" % (P2/1e5, (P1-P2)/1e5))
print("P3 = %g bar,  P2-P3 = %g bar" % (P3/1e5, (P2-P3)/1e5))



P2 = sqrt(P3**2 + C*V0**n)
P1 = sqrt(P2**2 + Ceq*V0**n)
print("\nb)")
print("P1 = %g bar" % (P1/1e5))
print("P2 = %g bar,  P1-P2 = %g bar" % (P2/1e5, (P1-P2)/1e5))
print("P3 = %g bar,  P2-P3 = %g bar" % (P3/1e5, (P2-P3)/1e5))



P2 = sqrt(P3**2 + Ceq*V0**n)
P1 = sqrt(P2**2 + C*V0**n)
print("\nc)")
print("P1 = %g bar" % (P1/1e5))
print("P2 = %g bar,  P1-P2 = %g bar" % (P2/1e5, (P1-P2)/1e5))
print("P3 = %g bar,  P2-P3 = %g bar" % (P3/1e5, (P2-P3)/1e5))





print("""
#######
##
## Resolução 2
##
#######
""")

T1 = 273.15 + 30
T2 = 273.15 + 25
T3 = 273.15 + 20


Tm12 = 0.5*(T1+T2)
Tm23 = 0.5*(T2+T3)

C12 = fCij (D, L, Tm12, d, mu)
C23 = fCij (D, L, Tm23, d, mu)
Ceq12 = C12**2/(2*C12**(1/n))**n
Ceq23 = C23**2/(2*C23**(1/n))**n


P2 = sqrt(P3**2 + C23*V0**n)
P1 = sqrt(P2**2 + C12*V0**n)
print("\na)")
print("P1 = %g bar" % (P1/1e5))
print("P2 = %g bar,  P1-P2 = %g bar" % (P2/1e5, (P1-P2)/1e5))
print("P3 = %g bar,  P2-P3 = %g bar" % (P3/1e5, (P2-P3)/1e5))



P2 = sqrt(P3**2 + C23*V0**n)
P1 = sqrt(P2**2 + Ceq12*V0**n)
print("\nb)")
print("P1 = %g bar" % (P1/1e5))
print("P2 = %g bar,  P1-P2 = %g bar" % (P2/1e5, (P1-P2)/1e5))
print("P3 = %g bar,  P2-P3 = %g bar" % (P3/1e5, (P2-P3)/1e5))



P2 = sqrt(P3**2 + Ceq23*V0**n)
P1 = sqrt(P2**2 + C12*V0**n)
print("\nc)")
print("P1 = %g bar" % (P1/1e5))
print("P2 = %g bar,  P1-P2 = %g bar" % (P2/1e5, (P1-P2)/1e5))
print("P3 = %g bar,  P2-P3 = %g bar" % (P3/1e5, (P2-P3)/1e5))




