#!/usr/bin/env python3
# vim: set fileencoding=utf-8 fileformat=unix :
# -*- coding: utf-8 -*-
# vim: set ts=8 et sw=4 sts=4 sta :
#######
##
## plotNetwork.py  --  Plot network graphs
##
## Author: Carlos Veiga Rodrigues <carlos.rodrigues@fe.up.pt>
## Copyright (C) 2018 Carlos Veiga Rodrigues <carlos.rodrigues@fe.up.pt>
## License: GPLv3 - GNU General Public License, version 3
##          <http://www.gnu.org/licenses/gpl-3.0.txt>
##
##   This program is free software: you can redistribute it and/or
##   modify it under the terms of the GNU General Public License
##   as published by the Free Software Foundation; either version 3
##   of the License, or (at your option) any later version.
##
##   This program is distributed in the hope that it will be useful,
##   but WITHOUT ANY WARRANTY; without even the implied warranty of
##   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
##   GNU General Public License for more details.
##
##   You should have received a copy of the GNU General Public License
##   along with this program. If not, see <http://www.gnu.org/licenses/>.
##
## This code includes adaptations from:
## netgraph.py --- Plot weighted, directed graphs of medium size (10-100 nodes)
## available at:  http://github.com/paulbrodersen/netgraph
##
#######

import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt


def makeAdjacency (ds, adjacencyMatrix=False, plot=True,\
    j='junction', s='connection', p='producer', c='consumer',\
    st='t_junction', sf='f_junction', sm='f', pj='qg_junc', cj='ql_junc', \
    **kwargs):
    """
    Plot using NetGraph Python code, see: http://github.com/paulbrodersen/netgraph
    """
    sKeys = sorted(ds[s].keys())
    Ns = len(sKeys)  # number of segments
    pKeys = sorted(ds[p].keys())
    Np = len(pKeys)  # number of producers
    cKeys = sorted(ds[c].keys())
    Nc = len(cKeys)  # number of consumers
    ## Get junction keys
    if j is None or j not in ds.keys():
        jKeys = []
        for k in sKeys:
            f, t = ds[s][k][sf], ds[s][k][st]
            jKeys.extend([f, t])
        jKeys = list(set(jKeys))  # a 'set' keeps only unique elements
    else:
        jKeys = sorted(ds[j].keys())
    Nj = len(jKeys)  # number of junctions
    ## Start mapping junction keys 
    a = {\
        'jkey': np.zeros(Nj, object),\
        'jk2n': {},\
        'kind': np.zeros(Nj, int),\
        'conn': np.zeros(Nj, int),
        }
    for (k, n,) in zip(jKeys, range(Nj)):
        a['jkey'][n] = int(k)
        a['jk2n'][int(k)] = n
    ## Number of connections
    for k in sKeys:
        f, t = int(ds[s][k][sf]), int(ds[s][k][st])
        a['conn'][a['jk2n'][f]] += 1
        a['conn'][a['jk2n'][t]] += 1
    ## Producers
    for k in pKeys:
        i = int(ds[p][k][pj])
        a['kind'][a['jk2n'][i]] = -1
    ## Consumers
    for k in cKeys:
        i = int(ds[c][k][cj])
        a['kind'][a['jk2n'][i]] = 1
    ## Reorder junctions producers -> nodes -> consumers
    w = (a['conn'] - min(a['conn'])) / (1. + max(a['conn']) - min(a['conn']))
    w += 10 * (a['kind'] - min(a['kind'])) \
        / (1. + max(a['kind']) - min(a['kind']))
    i = np.argsort(w)
    a['jkey'] = a['jkey'][i]
    a['kind'] = a['kind'][i]
    a['conn'] = a['conn'][i]
    for (k, n,) in zip(a['jkey'], range(Nj)):
        a['jk2n'][k] = n
    ## Create Adjacency Vector, AdjacencyVector = [source, target, weight]
    source, target, weight = [], [], []
    for k in ds[s].keys():
        f = int(ds[s][k][sf])
        t = int(ds[s][k][st])
        f = a['jk2n'][f]
        t = a['jk2n'][t]
        if sm is not None and sm in ds[s][k].keys():
            flow = ds[s][k][sm]
            if flow < 0:
                f, t = t, f
                flow = abs(flow)
        else:  # no flow key, weight number of connections instead
            flow = (a['conn'][f] + a['conn'][t]) / (2.*Nj)
        #A[f,t] = flow
        source.append(f)
        target.append(t)
        weight.append(flow)
    ## Create Adjacency Matrix, AdjacencyMatrix[source,target] = weight
    if adjacencyMatrix:
        A = np.zeros([Nj,Nj])
        for (f, t, w) in zip(source, target, weight):
            A[f, t] = w
    ## Alternatively, create Adjacency Matrix and:
    # source, target = np.where(A)
    # weight = A[source, target]
    # adjacencyVector = np.c_[sources, targets, weights]
    ## Node labels and colors for plot
    label = {}
    color = {}
    for n in range(Nj):
        label[n] = a['jkey'][n]
        if a['kind'][n] == -1:
            color[n] = 'goldenrod'
        elif a['kind'][n] == 1:
            color[n] = 'slateblue'
        else:
            color[n] = 'white'
    ## Node positions for plot, use new ordering as it is already weighted
    posxy = {}
    ## producers
    i = np.flatnonzero(a['kind'] == -1)
    if len(i) > 1:
        fy = 1 + np.log(len(i))/np.log(10)
        for (n, y) in zip(i, np.arange(len(i))/(len(i)-1.) - .5):
            posxy[n] = [-1, y*fy]
    elif len(i) == 1:
        posxy[i[0]] = (-1, 0)
    ## consumers
    i = np.flatnonzero(a['kind'] == 1)
    if len(i) > 1:
        fy = 1 + np.log(len(i))/np.log(10)
        for (n, y) in zip(i, np.arange(len(i))/(len(i)-1.) - .5):
            posxy[n] = [1, y*fy]
    elif len(i) == 1:
        posxy[i[0]] = [1, 0]
    ## others
    i = np.flatnonzero((a['kind'] != -1) & (a['kind'] != 1))
    #edge_list, edge_weight, is_directed = parse_graph(A)
    #edge_list = [(f, t) for (f, t,) in zip(source, target)]
    # xy = fruchterman_reingold_layout(edge_list, pos=posxy, fixed=posxy.keys())
    # if len(i) > 0:
    #     xmin, xmax = np.inf, -np.inf
    #     for n in i:
    #         xmin, xmax = min(xmin, xy[n][0]), max(xmax, xy[n][0])
    #     for (n, y) in zip(i, np.arange(len(i))/(len(i)-1.) - .5):
    #         x = (xy[n][0]-xmin)/float(xmax-xmin) * (.75 + .75) - .75
    #         posxy[n] = (x, 1.3*y)
    if len(i) > 0:
        ni = len(i)
        nxcol = ni // 6  # columns of 4 + 2
        nxrem = ni % 6
        nxtotal = 2*nxcol + int(nxrem > 0)
        if nxtotal > 1:
            x = np.arange(nxtotal)/(nxtotal-1.) * (.75 + .75) - .75
            nx = []
            for _ in range(nxcol):
                nx.extend([2, 4])
            if nxrem > nx[0]:
                nx.insert(0, nxrem)
            elif nxrem > 0:
                nx.append(nxrem)
        elif nxtotal == 1:
            nx, x = [nxrem], [0]
        else:
            nx, x = [], []
        n = 0
        for (nxx, xx) in zip(nx, x):
            if nxx > 1:
                y = np.arange(nxx)/(nxx-1.) - .5
                y *= nxx / 4. * 1.25
            else:
                y = np.array([0])
            for (ii, yy) in zip(i[n:n+nxx], y):
                posxy[ii] = (xx, yy)
                n+= 1
    ## Prepare output
    #node_labels = label
    #node_color = color
    #node_positions = posxy
    if adjacencyMatrix:
        adjacency = A
    else:
        adjacency = [(f, t, w,) for (f, t, w,) in zip(source, target, weight)]
    if plot:
        draw(adjacency, node_labels=label, node_color=color,\
            node_positions=posxy, draw_arrows=True, **kwargs)
        plt.tight_layout()
        plt.subplots_adjust(left=0, bottom=0, right=1, top=1, wspace=0, hspace=0)
        plt.margins(x=0, y=0)
    return adjacency, label, color, posxy


def plotAdjacency (adjacency, node_labels=None, node_color=None,\
    node_positions=None, draw_arrows=True, **kwargs):
    h = draw(adjacency, node_labels=node_labels, node_color=node_color,\
        node_positions=node_positions, draw_arrows=draw_arrows, **kwargs)
    return h



#######
##
## Start of netgraph.py adaptation. The following code was adapted from:
##
## netgraph.py --- Plot weighted, directed graphs of medium size (10-100 nodes)
##
## available at:  http://github.com/paulbrodersen/netgraph
##
## Author: Paul Brodersen <paulbrodersen+netgraph@gmail.com>
## Copyright (C) 2016 Paul Brodersen <paulbrodersen+netgraph@gmail.com>
## License: GNU GENERAL PUBLIC LICENSE Version 3
##
#######

BASE_NODE_SIZE = 1e-2
BASE_EDGE_WIDTH = 1e-2

def draw (graph, node_positions=None, node_labels=None, edge_labels=None,\
    edge_cmap='RdGy', ax=None, **kwargs):
    """
    Convenience function that tries to do "the right thing".

    For a full list of available arguments, and
    for finer control of the individual draw elements,
    please refer to the documentation of

        draw_nodes()
        draw_edges()
        draw_node_labels()
        draw_edge_labels()

    Arguments
    ----------
    graph: various formats
        Graph object to plot. Various input formats are supported.
        In order of precedence:
            - Edge list:
                Iterable of (source, target) or (source, target, weight) tuples,
                or equivalent (m, 2) or (m, 3) ndarray.
            - Adjacency matrix:
                Full-rank (n,n) ndarray, where n corresponds to the number of nodes.
                The absence of a connection is indicated by a zero.
            - igraph.Graph object
            - networkx.Graph object

    node_positions : dict node : (float, float)
        Mapping of nodes to (x, y) positions.
        If 'graph' is an adjacency matrix, nodes must be integers in range(n).

    node_labels : dict node : str (default None)
       Mapping of nodes to node labels.
       Only nodes in the dictionary are labelled.
       If 'graph' is an adjacency matrix, nodes must be integers in range(n).

    edge_labels : dict (source, target) : str (default None)
        Mapping of edges to edge labels.
        Only edges in the dictionary are labelled.

    ax : matplotlib.axis instance or None (default None)
       Axis to plot onto; if none specified, one will be instantiated with plt.gca().

    See Also
    --------
    draw_nodes()
    draw_edges()
    draw_node_labels()
    draw_edge_labels()
    """
    ## Accept a variety of formats and convert to common denominator
    edge_list, edge_weight, is_directed = parse_graph(graph)
    if edge_weight:
        ## If the graph is weighted, we want to visualise the weights using color.
        ## Edge width is another popular choice when visualising weighted networks,
        ## but if the variance in weights is large, this typically results in less
        ## visually pleasing results.
        edge_color  = get_color(edge_weight, cmap=edge_cmap)
        kwargs.setdefault('edge_color',  edge_color)
        ## Plotting darker edges over lighter edges typically results in visually
        ## more pleasing results. Here we hence specify the relative order in
        ## which edges are plotted according to the color of the edge.
        edge_zorder = _get_zorder(edge_color)
        kwargs.setdefault('edge_zorder', edge_zorder)
    ## Plot arrows if the graph has bi-directional edges
    if is_directed:
        kwargs.setdefault('draw_arrows', True)
    else:
        kwargs.setdefault('draw_arrows', False)
    ## Initialise node positions if none are given
    if node_positions is None:
        node_positions = fruchterman_reingold_layout(edge_list, **kwargs)
    elif len(node_positions) < len(_get_unique_nodes(edge_list)):
        ## some positions are given but not all
        node_positions = fruchterman_reingold_layout(edge_list,\
            pos=node_positions, fixed=node_positions.keys(), **kwargs)
    ## Create axis if none is given
    if ax is None:
        ax = plt.gca()
    ## Draw plot elements.
    draw_edges(edge_list, node_positions, ax=ax, **kwargs)
    draw_nodes(node_positions, ax=ax, **kwargs)
    ## This function needs to be called before any font sizes are adjusted
    if 'node_size' in kwargs:
        _update_view(node_positions, ax=ax, node_size=kwargs['node_size'])
    else:
        _update_view(node_positions, ax=ax)
    if node_labels is not None:
        if not 'node_label_font_size' in kwargs:
            ## set font size so even largest label fits in label face artist
            font_size = _get_font_size(ax, node_labels, **kwargs)
            font_size *= 0.9  # conservative fudge factor
            draw_node_labels(node_labels, node_positions,\
                node_label_font_size=font_size, ax=ax, **kwargs)
        else:
            draw_node_labels (node_labels, node_positions, ax=ax, **kwargs)
    if edge_labels is not None:
        draw_edge_labels (edge_list, edge_labels, node_positions, ax=ax,\
            **kwargs)
    ## Improve default layout of axis
    _make_pretty(ax)
    return ax


def parse_graph (graph):
    """
    Arguments
    ----------
    graph: various formats
        Graph object to plot. Various input formats are supported.
        In order of precedence:
            - Edge list:
                Iterable of (source, target) or (source, target, weight) tuples,
                or equivalent (m, 2) or (m, 3) ndarray.
            - Adjacency matrix:
                Full-rank (n,n) ndarray, where n corresponds to the number of nodes.
                The absence of a connection is indicated by a zero.

    Returns:
    --------
    edge_list: m-long list of 2-tuples
        List of edges. Each tuple corresponds to an edge defined by (source, target).

    edge_weights: dict (source, target) : float or None
        Edge weights. If the graph is unweighted, None is returned.

    is_directed: bool
        True, if the graph appears to be directed due to
            - the graph object class being passed in (e.g. a networkx.DiGraph), or
            - the existence of bi-directional edges.
    """
    if isinstance(graph, (list, tuple, set)):
        return _parse_sparse_matrix_format(graph)
    elif isinstance(graph, np.ndarray):
        rows, columns = graph.shape
        if columns in (2, 3):
            return _parse_sparse_matrix_format(graph)
        else:
            return _parse_adjacency_matrix(graph)
    else:
        allowed = ['list', 'tuple', 'set']
        raise NotImplementedError("Input graph must be one of: {}" \
            + "\nCurrently, type(graph) = {}".format(\
            "\n\n\t" + "\n\t".join(allowed)), type(graph))


def _parse_edge_list (edge_list):
    ## Edge list may be an array, or a list of lists.
    return [(source, target) for (source, target) in edge_list]


def _parse_sparse_matrix_format (adjacency):
    adjacency = np.array(adjacency)
    rows, columns = adjacency.shape
    if columns == 2:
        edge_list = _parse_edge_list(adjacency)
        return edge_list, None, _is_directed(edge_list)
    elif columns == 3:
        edge_list = _parse_edge_list(adjacency[:,:2])
        edge_weights = {(source, target) : weight for (source, target, weight) in adjacency}
        ## In a sparse adjacency format with weights,
        ## the type of nodes is promoted to the same type as weights,
        ## which is commonly a float. If all nodes can safely be demoted to ints,
        ## then we probably want to do that.
        tmp = [(\
            _save_cast_float_to_int(source),\
            _save_cast_float_to_int(target)) \
            for (source, target) in edge_list]
        if np.all([isinstance(num, int) for num in _flatten(tmp)]):
            edge_list = tmp
        if len(set(edge_weights.values())) > 1:
            return edge_list, edge_weights, _is_directed(edge_list)
        else:
            return edge_list, None, _is_directed(edge_list)
    else:
        raise ValueError("Graph specification in sparse matrix format"\
            + " needs to consist of an iterable of tuples of length 2 or 3."\
            + " Got iterable of tuples of length {}.".format(columns))


def _save_cast_float_to_int (num):
    if isinstance(num, float) and np.isclose(num, int(num)):
        return int(num)
    else:
        return num


def _parse_adjacency_matrix(adjacency):
    sources, targets = np.where(adjacency)
    edge_list = list(zip(sources.tolist(), targets.tolist()))
    edge_weights = {(source, target): adjacency[source, target] \
        for (source, target) in edge_list}
    if len(set(list(edge_weights.values()))) == 1:
        return edge_list, None, _is_directed(edge_list)
    else:
        return edge_list, edge_weights, _is_directed(edge_list)


def _is_directed(edge_list):
    ## test for bi-directional edges
    for (source, target) in edge_list:
        if ((target, source) in edge_list) and (source != target):
            return True
    return False


def get_color (mydict, cmap='RdGy', vmin=None, vmax=None):
    """
    Map positive and negative floats to a diverging colormap,
    such that
        1) the midpoint of the colormap corresponds to a value of 0., and
        2) values above and below the midpoint are mapped linearly and in equal measure
           to increases in color intensity.

    Arguments:
    ----------
    mydict: dict key : float
        Mapping of graph element (node, edge) to a float.
        For example (source, target) : edge weight.

    cmap: str
        Matplotlib colormap specification.

    vmin, vmax: float
        Minimum and maximum float corresponding to the dynamic range of the colormap.

    Returns:
    --------
    newdict: dict key : (float, float, float, float)
        Mapping of graph element to RGBA tuple.
    """
    keys = mydict.keys()
    values = np.array(list(mydict.values()), dtype=np.float64)
    ## apply edge_vmin, edge_vmax
    if vmin:
        values[values<vmin] = vmin
    if vmax:
        values[values>vmax] = vmax
    def abs(value):
        try:
            return np.abs(value)
        except TypeError as e: # value is probably None
            if isinstance(value, type(None)):
                return 0
            else:
                raise e
    ## rescale values such that
    ##  - the colormap midpoint is at zero-value, and
    ##  - negative and positive values have comparable intensity values
    values /= np.nanmax([np.nanmax(np.abs(values)), abs(vmax), abs(vmin)]) # [-1, 1]
    values += 1. # [0, 2]
    values /= 2. # [0, 1]
    ## convert value to color
    mapper = mpl.cm.ScalarMappable(cmap=cmap)
    mapper.set_clim(vmin=0., vmax=1.)
    colors = mapper.to_rgba(values)
    return {key: color for (key, color) in zip(keys, colors)}


def _get_zorder (color_dict):
    ## Reorder plot elements such that darker items are plotted last
    ## and hence most prominent in the graph
    ## Assuming colors are RGB
    zorder = np.argsort(np.sum(list(color_dict.values()), axis=1))  # assume RGB
    ## Reverse order as greater values correspond to lighter colors
    zorder = np.max(zorder) - zorder
    return {key: index for key, index in zip(color_dict.keys(), zorder)}


def draw_edges (edge_list, node_positions, node_size=3.,\
    edge_width=1., edge_color='k', edge_alpha=1., edge_zorder=None,\
    draw_arrows=True, ax=None, **kwargs):
    """
    Draw the edges of the network.

    Arguments
    ----------
    edge_list : m-long iterable of 2-tuples or equivalent (such as (m, 2) ndarray)
        List of edges. Each tuple corresponds to an edge defined by (source, target).

    node_positions : dict key : (float, float)
        Mapping of nodes to (x,y) positions

    node_size : scalar or (n,) or dict key : float (default 3.)
        Size (radius) of nodes.
        Used to offset edges when drawing arrow heads,
        such that the arrow heads are not occluded.
        If draw_nodes() and draw_edges() are called independently,
        make sure to set this variable to the same value.

    edge_width : float or dict (source, key) : width (default 1.)
        Line width of edges.
        NOTE: Value is rescaled by BASE_EDGE_WIDTH (1e-2) to work well with
              layout routines in igraph and networkx.

    edge_color : matplotlib color specification or
                 dict (source, target) : color specification (default 'k')
       Edge color.

    edge_alpha : float or dict (source, target) : float (default 1.)
        The edge transparency,

    edge_zorder : int or dict (source, target) : int (default None)
        Order in which to plot the edges.
        If None, the edges will be plotted in the order they appear in 'adjacency'.
        Note: graphs typically appear more visually pleasing if darker coloured edges
        are plotted on top of lighter coloured edges.

    draw_arrows : bool, optional (default True)
        If True, draws edges with arrow heads.

    ax : matplotlib.axis instance or None (default None)
       Axis to plot onto; if none specified, one will be instantiated with plt.gca().

    Returns
    -------
    artists: dict (source, target) : artist
        Mapping of edges to matplotlib.patches.FancyArrow artists.
    """
    if ax is None:
        ax = plt.gca()
    edge_list = [(source, target) for (source, target) in edge_list]
    nodes = node_positions.keys()
    number_of_nodes = len(nodes)
    # convert node and edge to dictionaries if they are not dictionaries already;
    # if dictionaries are provided, make sure that they are complete;
    # fill any missing entries with the default values
    if not isinstance(node_size, dict):
        node_size = {node:node_size for node in nodes}
    else:
        for node in nodes:
            node_size.setdefault(node, 3.)
    #
    if not isinstance(edge_width, dict):
        edge_width = {edge: edge_width for edge in edge_list}
    else:
        for edge in edge_list:
            edge_width.setdefault(edge, 1.)
    #
    if not isinstance(edge_color, dict):
        edge_color = {edge: edge_color for edge in edge_list}
    else:
        for edge in edge_list:
            edge_color.setdefault(edge, 'k')
    #
    if not isinstance(edge_alpha, dict):
        edge_alpha = {edge: edge_alpha for edge in edge_list}
    else:
        for edge in edge_list:
            edge_alpha.setdefault(edge, 1.)
    # rescale
    node_size  = {node: size  * BASE_NODE_SIZE  for (node, size)  in node_size.items()}
    edge_width = {edge: width * BASE_EDGE_WIDTH for (edge, width) in edge_width.items()}
    # order edges if necessary
    if edge_zorder:
        for edge in edge_list:
            edge_zorder.setdefault(edge, max(edge_zorder.values()))
        edge_list = sorted(edge_zorder, key=lambda k: edge_zorder[k])
    # NOTE: At the moment, only the relative zorder is honored, not the absolute value.
    artists = dict()
    for (source, target) in edge_list:
        if source != target:
            x1, y1 = node_positions[source]
            x2, y2 = node_positions[target]
            dx = x2 - x1
            dy = y2 - y1
            width = edge_width[(source, target)]
            color = edge_color[(source, target)]
            alpha = edge_alpha[(source, target)]
            if (target, source) in edge_list: # i.e. bidirectional
                # shift edge to the right (looking along the arrow)
                x1, y1, x2, y2 = _shift_edge(x1, y1, x2, y2, delta=0.5*width)
                # plot half arrow / line
                shape = 'right'
            else:
                shape = 'full'
            if draw_arrows:
                offset = node_size[target]
                head_length = 2 * width
                head_width = 3 * width
                length_includes_head = True
            else:
                offset = None
                head_length = 1e-10 # 0 throws error
                head_width = 1e-10 # 0 throws error
                length_includes_head = False
            patch = FancyArrow(x1, y1, dx, dy,
                               width=width,
                               facecolor=color,
                               alpha=alpha,
                               head_length=head_length,
                               head_width=head_width,
                               length_includes_head=length_includes_head,
                               zorder=1,
                               edgecolor='none',
                               linewidth=0.1,
                               offset=offset,
                               shape=shape)
            ax.add_artist(patch)
            artists[(source, target)] = patch
        else:  # source == target, i.e. a self-loop
            print("Warning: Plotting of self-loops not supported." \
                + " Ignoring edge ({}, {}).".format(source, target))
    return artists


def _shift_edge (x1, y1, x2, y2, delta):
    # get orthogonal unit vector
    v = np.r_[x2-x1, y2-y1] # original
    v = np.r_[-v[1], v[0]] # orthogonal
    v = v / np.linalg.norm(v) # unit
    dx, dy = delta * v
    return x1+dx, y1+dy, x2+dx, y2+dy


class FancyArrow (mpl.patches.Polygon):
    """
    This is an expansion of of matplotlib.patches.FancyArrow.
    """
    _edge_default = True
    def __str__(self):
        return "FancyArrow()"
    def __init__(self, x, y, dx, dy, width=0.001, length_includes_head=False,
                 head_width=None, head_length=None, shape='full', overhang=0,
                 head_starts_at_zero=False, offset=None, **kwargs):
        """
        Constructor arguments
          *width*: float (default: 0.001)
            width of full arrow tail

          *length_includes_head*: [True | False] (default: False)
            True if head is to be counted in calculating the length.

          *head_width*: float or None (default: 3*width)
            total width of the full arrow head

          *head_length*: float or None (default: 1.5 * head_width)
            length of arrow head

          *shape*: ['full', 'left', 'right'] (default: 'full')
            draw the left-half, right-half, or full arrow

          *overhang*: float (default: 0)
            fraction that the arrow is swept back (0 overhang means
            triangular shape). Can be negative or greater than one.

          *head_starts_at_zero*: [True | False] (default: False)
            if True, the head starts being drawn at coordinate 0
            instead of ending at coordinate 0.

        Other valid kwargs (inherited from :class:`Patch`) are:
        %(Patch)s
        """
        self.width = width
        if head_width is None:
            self.head_width = 3 * self.width
        else:
            self.head_width = head_width
        if head_length is None:
            self.head_length = 1.5 * self.head_width
        else:
            self.head_length = head_length
        self.length_includes_head = length_includes_head
        self.head_starts_at_zero = head_starts_at_zero
        self.overhang = overhang
        self.shape = shape
        self.offset = offset
        verts = self.compute_vertices(x, y, dx, dy)
        mpl.patches.Polygon.__init__(self, list(map(tuple, verts)), closed=True, **kwargs)
    def compute_vertices (self, x, y, dx, dy):
        distance = np.hypot(dx, dy)
        if self.offset:
            dx *= (distance-self.offset)/distance
            dy *= (distance-self.offset)/distance
            distance = np.hypot(dx, dy)
            # distance -= self.offset
        if self.length_includes_head:
            length = distance
        else:
            length = distance + self.head_length
        if not length:
            verts = []  # display nothing if empty
        else:
            # start by drawing horizontal arrow, point at (0,0)
            hw, hl, hs, lw = self.head_width, self.head_length, self.overhang, self.width
            left_half_arrow = np.array([
                [0.0, 0.0],                   # tip
                [-hl, -hw / 2.0],             # leftmost
                [-hl * (1 - hs), -lw / 2.0],  # meets stem
                [-length, -lw / 2.0],         # bottom left
                [-length, 0],
            ])
            # if we're not including the head, shift up by head length
            if not self.length_includes_head:
                left_half_arrow += [self.head_length, 0]
            # if the head starts at 0, shift up by another head length
            if self.head_starts_at_zero:
                left_half_arrow += [self.head_length / 2.0, 0]
            # figure out the shape, and complete accordingly
            if self.shape == 'left':
                coords = left_half_arrow
            else:
                right_half_arrow = left_half_arrow * [1, -1]
                if self.shape == 'right':
                    coords = right_half_arrow
                elif self.shape == 'full':
                    # The half-arrows contain the midpoint of the stem,
                    # which we can omit from the full arrow. Including it
                    # twice caused a problem with xpdf.
                    coords = np.concatenate([left_half_arrow[:-1],
                                             right_half_arrow[-1::-1]])
                else:
                    raise ValueError("Got unknown shape: %s" % self.shape)
            if distance != 0:
                cx = float(dx) / distance
                sx = float(dy) / distance
            else:
                #Account for division by zero
                cx, sx = 0, 1
            M = np.array([[cx, sx], [-sx, cx]])
            verts = np.dot(coords, M) + (x + dx, y + dy)
        return verts
    def update_vertices(self, x0, y0, dx, dy):
        verts = self.compute_vertices(x0, y0, dx, dy)
        if getattr(self, "set_xy", None) is None:
            self._set_xy(verts)
        else:
            self.set_xy(verts)

def draw_nodes(node_positions, node_shape='o', node_size=3.,\
    node_edge_width=0.5, node_color='w', node_edge_color='k',\
    node_alpha=1.0, node_edge_alpha=1.0, ax=None, **kwargs):
    """
    Draw node markers at specified positions.

    Arguments
    ----------
    node_positions : dict node : (float, float)
        Mapping of nodes to (x, y) positions.

    node_shape : string or dict key : string (default 'o')
       The shape of the node. Specification is as for matplotlib.scatter
       marker, i.e. one of 'so^>v<dph8'.
       If a single string is provided all nodes will have the same shape.

    node_size : scalar or dict node : float (default 3.)
       Size (radius) of nodes.
       NOTE: Value is rescaled by BASE_NODE_SIZE (1e-2) to work well with layout routines in igraph and networkx.

    node_edge_width : scalar or dict key : float (default 0.5)
       Line width of node marker border.
       NOTE: Value is rescaled by BASE_NODE_SIZE (1e-2) to work well with layout routines in igraph and networkx.

    node_color : matplotlib color specification or dict node : color specification (default 'w')
       Node color.

    node_edge_color : matplotlib color specification or dict node : color specification (default 'k')
       Node edge color.

    node_alpha : scalar or dict node : float (default 1.)
       The node transparency.

    node_edge_alpha : scalar or dict node : float (default 1.)
       The node edge transparency.

    ax : matplotlib.axis instance or None (default None)
       Axis to plot onto; if none specified, one will be instantiated with plt.gca().

    Returns
    -------
    node_faces: dict node : artist
        Mapping of nodes to the node face artists.

    node_edges: dict node : artist
        Mapping of nodes to the node edge artists.
    """
    if ax is None:
        ax = plt.gca()
    # convert all inputs to dicts mapping node:property
    nodes = node_positions.keys()
    number_of_nodes = len(nodes)
    if isinstance(node_size, (int, float)):
        node_size = {node:node_size for node in nodes}
    if isinstance(node_edge_width, (int, float)):
        node_edge_width = {node: node_edge_width for node in nodes}
    # Simulate node edge by drawing a slightly larger node artist.
    # I wish there was a better way to do this,
    # but this seems to be the only way to guarantee constant proportions,
    # as linewidth argument in matplotlib.patches will not be proportional
    # to a given node radius.
    node_edges = _draw_nodes(node_positions,\
        node_shape=node_shape,\
        node_size=node_size,\
        node_color=node_edge_color,\
        node_alpha=node_edge_alpha,\
        ax=ax,\
        **kwargs)
    node_size = {n: node_size[n] - node_edge_width[n] for n in nodes}
    node_faces = _draw_nodes(node_positions,\
        node_shape=node_shape,\
        node_size=node_size,\
        node_color=node_color,\
        node_alpha=node_alpha,\
        ax=ax,\
        **kwargs)
    return node_faces, node_edges


def _draw_nodes (node_positions, node_shape='o', node_size=3., node_color='r',\
    node_alpha=1.0, ax=None, **kwargs):
    """
    Draw node markers at specified positions.

    Arguments
    ----------
    node_positions : dict node : (float, float)
        Mapping of nodes to (x, y) positions

    node_shape : string or dict key : string (default 'o')
       The shape of the node. Specification is as for matplotlib.scatter
       marker, i.e. one of 'so^>v<dph8'.
       If a single string is provided all nodes will have the same shape.

    node_size : scalar or dict node : float (default 3.)
       Size (radius) of nodes.
       NOTE: Value is rescaled by BASE_NODE_SIZE (1e-2) to work well with layout routines in igraph and networkx.

    node_color : matplotlib color specification or dict node : color specification (default 'w')
       Node color.

    node_alpha : scalar or dict node : float (default 1.)
       The node transparency.

    ax : matplotlib.axis instance or None (default None)
       Axis to plot onto; if none specified, one will be instantiated with plt.gca().

    Returns
    -------
    artists: dict node : artist
        Mapping of nodes to the artists,
    """
    if ax is None:
        ax = plt.gca()
    # convert all inputs to dicts mapping node:property
    nodes = node_positions.keys()
    number_of_nodes = len(nodes)
    if isinstance(node_shape, str):
        node_shape = {node:node_shape for node in nodes}
    if not isinstance(node_color, dict):
        node_color = {node:node_color for node in nodes}
    if isinstance(node_alpha, (int, float)):
        node_alpha = {node:node_alpha for node in nodes}
    # rescale
    node_size = {node: size  * BASE_NODE_SIZE for (node, size)  in node_size.items()}
    artists = dict()
    for node in nodes:
        node_artist = _get_node_artist(\
            shape=node_shape[node],\
            position=node_positions[node],\
            size=node_size[node],\
            facecolor=node_color[node],\
            alpha=node_alpha[node],\
            zorder=2)
        # add artists to axis
        ax.add_artist(node_artist)
        # return handles to artists
        artists[node] = node_artist
    return artists


class RegularPolygon (mpl.patches.RegularPolygon):
    """
    The API for matplotlib.patches.Circle and matplotlib.patches.RegularPolygon
    in matplotlib differ substantially.  This class tries to bridge some of
    these gaps by translating Circle methods into RegularPolygon methods.
    """
    #center = property(\
    #    mpl.patches.RegularPolygon._get_xy,\
    #    mpl.patches.RegularPolygon._set_xy)
    get_xy = getattr(mpl.patches.RegularPolygon, "get_xy", None)
    set_xy = getattr(mpl.patches.RegularPolygon, "set_xy", None)
    if get_xy is None or set_xy is None:
        get_xy = getattr(mpl.patches.RegularPolygon, "_get_xy", None)
        set_xy = getattr(mpl.patches.RegularPolygon, "_set_xy", None)
    if get_xy is None or set_xy is None:
        get_xy = getattr(mpl.patches.RegularPolygon, "xy", None)
        set_xy = getattr(mpl.patches.RegularPolygon, "xy", None)
    center = property(get_xy, set_xy)


def _get_node_artist (shape, position, size, facecolor, alpha, zorder=2):
    kwargs = {\
        'radius':size, 'facecolor':facecolor, 'alpha':alpha, 'zorder':zorder}
    if shape == 'o':  # circle
        artist = mpl.patches.Circle(xy=position, linewidth=0., **kwargs)
    elif shape == '^':  # triangle up
        artist = RegularPolygon(xy=position,\
            numVertices=3, orientation=0, linewidth=0., **kwargs)
    elif shape == '<':  # triangle left
        artist = RegularPolygon(xy=position,\
            numVertices=3, orientation=np.pi*0.5, linewidth=0., **kwargs)
    elif shape == 'v':  # triangle down
        artist = RegularPolygon(xy=position,\
            numVertices=3, orientation=np.pi, linewidth=0., **kwargs)
    elif shape == '>':  # triangle right
        artist = RegularPolygon(xy=position,\
            numVertices=3, orientation=np.pi*1.5, linewidth=0., **kwargs)
    elif shape == 's':  # square
        artist = RegularPolygon(xy=position,\
            numVertices=4, orientation=np.pi*0.25, linewidth=0., **kwargs)
    elif shape == 'd':  # diamond
        artist = RegularPolygon(xy=position,\
            numVertices=4, orientation=np.pi*0.5, linewidth=0., **kwargs)
    elif shape == 'p':  # pentagon
        artist = RegularPolygon(xy=position,\
            numVertices=5, linewidth=0., **kwargs)
    elif shape == 'h':  # hexagon
        artist = RegularPolygon(xy=position,\
            numVertices=6, linewidth=0., **kwargs)
    elif shape == 8: # octagon
        artist = RegularPolygon(xy=position,\
            numVertices=8, linewidth=0., **kwargs)
    else:
        raise ValueError("Node shape one of: ''so^>v<dph8'."\
                + " Current shape:{}".format(shape))
    return artist


def _update_view(node_positions, ax, node_size=3.):
    # Pad x and y limits as patches are not registered properly
    # when matplotlib sets axis limits automatically.
    # Hence we need to set them manually.
    if isinstance(node_size, dict):
        maxs = np.max(list(node_size.values())) * BASE_NODE_SIZE
    else:
        maxs = node_size * BASE_NODE_SIZE
    maxx, maxy = np.max(list(node_positions.values()), axis=0)
    minx, miny = np.min(list(node_positions.values()), axis=0)
    w = maxx-minx
    h = maxy-miny
    padx, pady = 0.05*w + maxs, 0.05*h + maxs
    corners = (minx-padx, miny-pady), (maxx+padx, maxy+pady)
    ax.update_datalim(corners)
    ax.autoscale_view()
    ax.get_figure().canvas.draw()
    return


def _get_font_size (ax, node_labels, **kwargs):
    """
    Determine the maximum font size that results in labels that still all fit
    inside the node face artist.

    TODO:
    -----
    - add font / fontfamily as optional argument
    - potentially, return a dictionary of font sizes instead; then rescale
      font sizes individually on a per node basis
    """
    # check if there are relevant parameters in kwargs:
    #   - node size,
    #   - edge width, or
    #   - node_label_font_size
    if 'node_size' in kwargs:
        node_size = kwargs['node_size']
    else:
        node_size = 3.  # default
    if 'node_edge_width' in kwargs:
        node_edge_width = kwargs['node_edge_width']
    else:
        node_edge_width = 0.5  # default
    if 'node_label_font_size' in kwargs:
        node_label_font_size = kwargs['node_label_font_size']
    else:
        node_label_font_size = 12.  # default
    ## find widest node label; use its rescale factor to set font size for all
    widest = 0.
    rescale_factor = np.nan
    for key, label in node_labels.items():
        if isinstance(node_size, (int, float)):
            r = node_size
        elif isinstance(node_size, dict):
            r = node_size[key]
        if isinstance(node_edge_width, (int, float)):
            e = node_edge_width
        elif isinstance(node_edge_width, dict):
            e = node_edge_width[key]
        node_diameter = 2 * (r-e) * BASE_NODE_SIZE
        width, height = _get_text_object_dimentions(\
            ax, label, size=node_label_font_size)
        if width > widest:
            widest = width
            rescale_factor = node_diameter / np.sqrt(width**2 + height**2)
    font_size = node_label_font_size * rescale_factor
    return font_size


def draw_node_labels (node_labels, node_positions, node_label_font_size=12,\
    node_label_font_color='k', node_label_font_family='sans-serif',\
    node_label_font_weight='normal', node_label_font_alpha=1.,\
    node_label_bbox=dict(alpha=0.), node_label_horizontalalignment='center',\
    node_label_verticalalignment='center', node_label_offset=(0., 0.),\
    clip_on=False, ax=None, **kwargs):
    """
    Draw node labels.

    Arguments
    ---------
    node_positions : dict node : (float, float)
        Mapping of nodes to (x, y) positions

    node_labels : dict key : str
       Mapping of nodes to labels.
       Only nodes in the dictionary are labelled.

    node_label_font_size : int (default 12)
       Font size for text labels

    node_label_font_color : str (default 'k')
       Font color string

    node_label_font_family : str (default='sans-serif')
       Font family

    node_label_font_weight : str (default='normal')
       Font weight

    node_label_font_alpha : float (default 1.)
       Text transparency

    node_label_bbox : matplotlib bbox instance (default {'alpha': 0})
       Specify text box shape and colors.

    node_label_horizontalalignment: str
        Horizontal label alignment inside bbox.

    node_label_verticalalignment: str
        Vertical label alignment inside bbox.

    node_label_offset: 2-tuple or equivalent iterable (default (0.,0.))
        (x, y) offset from node centre of label position.

    clip_on : bool (default False)
       Turn on clipping at axis boundaries.

    ax : matplotlib.axis instance or None (default None)
       Axis to plot onto; if none specified, one will be instantiated with plt.gca().

    Returns
    -------
    artists: dict
        Dictionary mapping node indices to text objects.

    @reference
    Borrowed with minor modifications from networkx/drawing/nx_pylab.py
    """
    if ax is None:
        ax = plt.gca()
    dx, dy = node_label_offset
    artists = dict()  # there is no text collection so we'll fake one
    for node, label in node_labels.items():
        try:
            x, y = node_positions[node]
        except KeyError:
            print("Cannot draw node label for node with ID {}."\
                + "The node has no position assigned to it.".format(node))
            continue
        x += dx
        y += dy
        text_object = ax.text(x, y, label,\
            size=node_label_font_size,\
            color=node_label_font_color,\
            alpha=node_label_font_alpha,\
            family=node_label_font_family,\
            weight=node_label_font_weight,\
            bbox=node_label_bbox,\
            horizontalalignment=node_label_horizontalalignment,\
            verticalalignment=node_label_verticalalignment,\
            transform=ax.transData,\
            clip_on=clip_on)
        artists[node] = text_object
    return artists


def _get_text_object_dimentions (ax, string, *args, **kwargs):
    text_object = ax.text(0., 0., string, *args, **kwargs)
    renderer = _find_renderer(text_object.get_figure())
    bbox_in_display_coordinates = text_object.get_window_extent(renderer)
    bbox_in_data_coordinates = bbox_in_display_coordinates.transformed(ax.transData.inverted())
    w, h = bbox_in_data_coordinates.width, bbox_in_data_coordinates.height
    text_object.remove()
    return w, h


def _find_renderer (fig):
    """
    stackoverflow.com/questions/22667224/matplotlib-get-text-bounding-box-independent-of-backend
    """
    if hasattr(fig.canvas, "get_renderer"):
        # Some backends, such as TkAgg, have the get_renderer method, which
        # makes this easy.
        renderer = fig.canvas.get_renderer()
    else:
        # Other backends do not have the get_renderer method, so we have a work
        # around to find the renderer. Print the figure to a temporary file
        # object, and then grab the renderer that was used.
        # (I stole this trick from the matplotlib backend_bases.py
        # print_figure() method.)
        import io
        fig.canvas.print_pdf(io.BytesIO())
        renderer = fig._cachedRenderer
    return(renderer)


def draw_edge_labels (edge_list, edge_labels, node_positions,\
    edge_label_position=0.5, edge_label_font_size=10,\
    edge_label_font_color='k', edge_label_font_family='sans-serif',\
    edge_label_font_weight='normal', edge_label_font_alpha=1.,\
    edge_label_bbox=None, edge_label_horizontalalignment='center',\
    edge_label_verticalalignment='center', clip_on=False, edge_width=1.,\
    ax=None, rotate=True, edge_label_zorder=10000, **kwargs):
    """
    Draw edge labels.

    Arguments
    ---------

    edge_list: m-long list of 2-tuples
        List of edges. Each tuple corresponds to an edge defined by (source, target).

    edge_labels : dict (source, target) : str
        Mapping of edges to edge labels.
        Only edges in the dictionary are labelled.

    node_positions : dict node : (float, float)
        Mapping of nodes to (x, y) positions

    edge_label_position : float
        Relative position along the edge where the label is placed.
            head   : 0.
            centre : 0.5 (default)
            tail   : 1.

    edge_label_font_size : int (default 12)
       Font size

    edge_label_font_color : str (default 'k')
       Font color

    edge_label_font_family : str (default='sans-serif')
       Font family

    edge_label_font_weight : str (default='normal')
       Font weight

    edge_label_font_alpha : float (default 1.)
       Text transparency

    edge_label_bbox : Matplotlib bbox
       Specify text box shape and colors.

    edge_label_horizontalalignment: str
        Horizontal label alignment inside bbox.

    edge_label_verticalalignment: str
        Vertical label alignment inside bbox.

    clip_on : bool (default=False)
       Turn on clipping at axis boundaries.

    edge_label_zorder : int (default 10000)
        Set the zorder of edge labels.
        Choose a large number to ensure that the labels are plotted on top of the edges.

    edge_width : float or dict (source, key) : width (default 1.)
        Line width of edges.
        NOTE: Value is rescaled by BASE_EDGE_WIDTH (1e-2) to work well with layout
              routines in igraph and networkx.

    ax : matplotlib.axis instance or None (default None)
       Axis to plot onto; if none specified, one will be instantiated with plt.gca().

    Returns
    -------
    artists: dict (source, target) : text object
        Mapping of edges to edge label artists.

    @reference
    Borrowed with minor modifications from networkx/drawing/nx_pylab.py
    """
    if ax is None:
        ax = plt.gca()
    if isinstance(edge_width, (int, float)):
        edge_width = {edge: edge_width for edge in edge_list}
    edge_width = {edge: width * BASE_EDGE_WIDTH \
        for (edge, width) in edge_width.items()}
    text_items = {}
    for (n1, n2), label in edge_labels.items():
        if n1 != n2:
            (x1, y1) = node_positions[n1]
            (x2, y2) = node_positions[n2]
            if (n2, n1) in edge_list:
                ## i.e. bidirectional edge, need to shift label to stay on edge
                x1, y1, x2, y2 = _shift_edge(\
                    x1, y1, x2, y2, delta=1.*edge_width[(n1, n2)])
            (x, y) = (x1*edge_label_position + x2*(1. - edge_label_position),\
                      y1*edge_label_position + y2*(1. - edge_label_position))
            if rotate:
                angle = np.arctan2(y2-y1, x2-x1)/(2.0*np.pi)*360  # degrees
                ## make label orientation "right-side-up"
                if angle > 90:
                    angle -= 180
                if angle < - 90:
                    angle += 180
                ## transform data coordinate angle to screen coordinate angle
                xy = np.array((x, y))
                trans_angle = ax.transData.transform_angles(\
                    np.array((angle,)), xy.reshape((1, 2)))[0]
            else:
                trans_angle = 0.0
            if edge_label_bbox is None:  # use default box of white with white border
                edge_label_bbox = dict(boxstyle='round',\
                    ec=(1.0, 1.0, 1.0),\
                    fc=(1.0, 1.0, 1.0))
            t = ax.text(x, y, label,\
                size=edge_label_font_size,\
                color=edge_label_font_color,\
                family=edge_label_font_family,\
                weight=edge_label_font_weight,\
                bbox=edge_label_bbox,\
                horizontalalignment=edge_label_horizontalalignment,\
                verticalalignment=edge_label_verticalalignment,\
                rotation=trans_angle,\
                transform=ax.transData,\
                zorder=edge_label_zorder,\
                clip_on=clip_on)
            text_items[(n1, n2)] = t
        else:  # n1 == n2, i.e. a self-loop
            print("Warning:"\
                + " Plotting of edge labels for self-loops not supported."\
                + " Ignoring edge with label: {}".format(label))
    return text_items


def fruchterman_reingold_layout (edge_list,\
    edge_weights=None, k=None, pos=None, fixed=None,\
    iterations=50, scale=1, center=np.zeros((2)), dim=2, **kwargs):
    """
    Position nodes using Fruchterman-Reingold force-directed algorithm.

    Parameters
    ----------
    edge_list: m-long iterable of 2-tuples or equivalent (such as (m, 2) ndarray)
        List of edges. Each tuple corresponds to an edge defined by (source, target).

    edge_weights: dict (source, target) : float or None (default=None)
        Edge weights.

    k : float (default=None)
        Optimal distance between nodes.  If None the distance is set to
        1/sqrt(n) where n is the number of nodes.  Increase this value
        to move nodes farther apart.

    pos : dict or None  optional (default=None)
        Initial positions for nodes as a dictionary with node as keys
        and values as a coordinate list or tuple.  If None, then use
        random initial positions.

    fixed : list or None  optional (default=None)
        Nodes to keep fixed at initial position.

    iterations : int  optional (default=50)
        Number of iterations of spring-force relaxation

    scale : number (default: 1)
        Scale factor for positions. Only used if `fixed is None`.

    center : array-like or None
        Coordinate pair around which to center the layout.
        Only used if `fixed is None`.

    dim : int
        Dimension of layout.

    Returns
    -------
    pos : dict
        A dictionary of positions keyed by node

    Notes:
    ------
    Implementation taken with minor modifications from networkx.spring_layout().
    """
    nodes = _get_unique_nodes(edge_list)
    total_nodes = len(nodes)
    ## translate fixed node ID to position in node list
    if fixed is not None:
        node_to_idx = dict(zip(nodes, range(total_nodes)))
        fixed = np.asarray([node_to_idx[v] for v in fixed])
    if pos is not None:
        # Determine size of existing domain to adjust initial positions
        domain_size = max(coord for pos_tup in pos.values() for coord in pos_tup)
        if domain_size == 0:
            domain_size = 1
        shape = (total_nodes, dim)
        pos_arr = np.random.random(shape) * domain_size + center
        for i, n in enumerate(nodes):
            if n in pos:
                pos_arr[i] = np.asarray(pos[n])
    else:
        pos_arr = None
    if k is None and fixed is not None:
        ## We must adjust k by domain size for layouts not near 1x1
        k = domain_size / np.sqrt(total_nodes)
    A = _edge_list_to_adjacency(edge_list, edge_weights)
    pos = _dense_fruchterman_reingold(A, k, pos_arr, fixed, iterations, dim)
    if fixed is None:
        pos = _rescale_layout(pos, scale=scale) + center
    return dict(zip(nodes, pos))


def _dense_fruchterman_reingold (A, k=None, pos=None, fixed=None,\
    iterations=50, dim=2):
    """
    Position nodes in adjacency matrix A using Fruchterman-Reingold
    """
    nnodes, _ = A.shape
    if pos is None:
        pos = np.random.rand(nnodes, dim)  # random initial positions
    if k is None:
        k = np.sqrt(1.0/nnodes)  # optimal distance between nodes
    ## the initial "temperature"  is about .1 of domain area (=1x1)
    ## this is the largest step allowed in the dynamics.
    ## We need to calculate this in case our fixed positions force our domain
    ## to be much bigger than 1x1
    t = max(max(pos.T[0]) - min(pos.T[0]), max(pos.T[1]) - min(pos.T[1]))*0.1
    # simple cooling scheme.
    # linearly step down by dt on each iteration so last iteration is size dt.
    dt = t/float(iterations+1)
    delta = np.zeros((pos.shape[0], pos.shape[0], pos.shape[1]), dtype=A.dtype)
    # the inscrutable (but fast) version
    # this is still O(V^2)
    # could use multilevel methods to speed this up significantly
    for iteration in range(iterations):
        # matrix of difference between points
        delta = pos[:, np.newaxis, :] - pos[np.newaxis, :, :]
        # distance between points
        distance = np.linalg.norm(delta, axis=-1)
        # enforce minimum distance of 0.01
        np.clip(distance, 0.01, None, out=distance)
        # displacement "force"
        displacement = np.einsum('ijk,ij->ik', delta,\
            (k * k / distance**2 - A * distance / k))
        # update positions
        length = np.linalg.norm(displacement, axis=-1)
        length = np.where(length < 0.01, 0.1, length)
        delta_pos = np.einsum('ij,i->ij', displacement, t / length)
        if fixed is not None:
            # don't change positions of fixed nodes
            delta_pos[fixed] = 0.0
        pos += delta_pos
        # cool temperature
        t -= dt
    return pos


def _rescale_layout (pos, scale=1):
    """
    Return scaled position array to (-scale, scale) in all axes.
    The function acts on NumPy arrays which hold position information.
    Each position is one row of the array. The dimension of the space
    equals the number of columns. Each coordinate in one column.

    To rescale, the mean (center) is subtracted from each axis separately.
    Then all values are scaled so that the largest magnitude value
    from all axes equals `scale` (thus, the aspect ratio is preserved).
    The resulting NumPy Array is returned (order of rows unchanged).

    Parameters
    ----------
    pos : numpy array
        positions to be scaled. Each row is a position.

    scale : number (default: 1)
        The size of the resulting extent in all directions.

    Returns
    -------
    pos : numpy array
        scaled positions. Each row is a position.
    """
    # Find max length over all dimensions
    lim = 0  # max coordinate for all axes
    for i in range(pos.shape[1]):
        pos[:, i] -= pos[:, i].mean()
        lim = max(abs(pos[:, i]).max(), lim)
    # rescale to (-scale, scale) in all directions, preserves aspect
    if lim > 0:
        for i in range(pos.shape[1]):
            pos[:, i] *= scale / lim
    return pos


def _flatten(nested_list):
    return [item for sublist in nested_list for item in sublist]


def _get_unique_nodes(edge_list):
    """
    Using numpy.unique promotes nodes to numpy.float/numpy.int/numpy.str,
    and breaks for nodes that have a more complicated type such as a tuple.
    """
    return list(set(_flatten(edge_list)))


def _edge_list_to_adjacency (edge_list, edge_weights=None):
    sources = [s for (s, _) in edge_list]
    targets = [t for (_, t) in edge_list]
    if edge_weights:
        weights = [edge_weights[edge] for edge in edge_list]
    else:
        weights = np.ones((len(edge_list)))
    ## map nodes to consecutive integers
    nodes = sources + targets
    unique, indices = np.unique(nodes, return_inverse=True)
    node_to_idx = dict(zip(unique, indices))
    source_indices = [node_to_idx[source] for source in sources]
    target_indices = [node_to_idx[target] for target in targets]
    total_nodes = len(unique)
    adjacency_matrix = np.zeros((total_nodes, total_nodes))
    adjacency_matrix[source_indices, target_indices] = weights
    return adjacency_matrix


def _make_pretty(ax):
    ax.set_xticks([])
    ax.set_yticks([])
    ax.set_aspect('equal')
    ax.get_figure().set_facecolor('w')
    ax.set_frame_on(False)
    ax.get_figure().canvas.draw()
    return

#######
##
## End of netgraph.py
##
#######



def plotUsingNetworkX (ds, j='junction', s='connection', p='producer', c='consumer',\
    st='t_junction', sf='f_junction', sm='f', pj='qg_junc', cj='ql_junc'):
    """
    Plot using NetworkX Python module, see:
    http://python-graph-gallery.com/324-map-a-color-to-network-nodes/
    http://networkx.github.io/documentation/networkx-1.10/tutorial/tutorial.html
    """
    import networkx as nx
    G = nx.Graph()
    ## Add a list of nodes
    if j is None or j not in ds.keys():
        junctions = []
        for k in sorted(ds[s].keys()):
            f, t = ds[s][k][sf], ds[s][k][st]
            junctions.extend([f, t])
        junctions = list(set(junctions))  # a 'set' keeps only unique elements
    else:
        jKeys = sorted(ds[j].keys())
        junctions = list(ds[j].keys())
    junctions = [int(junction) for junction in junctions]
    junctions.sort()
    G.add_nodes_from(junctions)
    ## Add color
    for junction in G.node.keys():
        G.node[junction]['color'] = 'gray'
    ## Add edges
    for k in ds[s].keys():
        f = int(ds[s][k][sf])
        t = int(ds[s][k][st])
        if 'length' in ds[s][k].keys():
            L = float(ds[s][k]['length'])
            G.add_edge(f, t, weight=L)
        else:
            G.add_edge(f, t)
    #print(G.nodes())
    #print(G.edges())
    ## Add producers
    for k in ds[p]:
        junction = int(ds[p][k][pj])
        G.node[junction]['color'] = 'skyblue'
    ## Add consumers
    for k in ds[c]:
        junction = int(ds[c][k][cj])
        G.node[junction]['color'] = 'goldenrod'
    ## Colors
    color = []
    for node in G:
        color.append(G.node[node]['color'])
    #pos = nx.spring_layout(G, scale=3)
    #nx.draw(G, pos, node_color=color, with_labels=True, font_size=6)
    draw_function = nx.draw
    #draw_function = nx.draw_random
    #draw_function = nx.draw_circular
    #draw_function = nx.draw_spectral
    #draw_function = nx.draw_spring
    #draw_function = nx.draw_shell
    draw_function(G, node_color=color, with_labels=True, font_size=6)
    plt.show()
    return



#######
##
## Execucao
##
#######
if __name__ == '__main__':
    ## Test 1
    ds = {\
        'j':{1:{}, 2:{}, 3:{}, 4:{}, 5:{}},\
        's':{\
            13:{'f':1, 't':3},\
            14:{'f':1, 't':4},\
            23:{'f':2, 't':3},\
            34:{'f':3, 't':4},\
            45:{'f':4, 't':5},\
            25:{'f':2, 't':5},\
        },\
        'p':{1001:{'p':1}, 1002:{'p':2}},\
        'c':{2005:{'c':5}},\
        }
    adjacency, label, color, posxy = makeAdjacency (ds, plot=False,\
        j='j', s='s', p='p', c='c', st='t', sf='f', sm='flow', pj='p', cj='c')
    h = plotAdjacency (adjacency, node_labels=label, node_color=color,\
        node_positions=posxy)
    plt.show()
    ## Test 2.1
    ds = {\
        'j':{0:{}, 1:{}, 2:{}, 3:{}, 4:{}, 5:{}, 6:{}},\
        's':{\
            '02':{'f':0, 't':2},\
            '03':{'f':0, 't':3},\
            '12':{'f':1, 't':2},\
            '23':{'f':2, 't':3},\
            '34':{'f':3, 't':4},\
            '14':{'f':1, 't':4},\
            '53':{'f':5, 't':3},\
            '36':{'f':3, 't':6},\
        },\
        'p':{'100':{'p':0}, '101':{'p':1}, '105':{'p':5}},\
        'c':{'204':{'c':4}, '206':{'c':6}},\
        }
    adjacency, label, color, posxy = makeAdjacency (ds, adjacencyMatrix=True,\
        j=None, s='s', p='p', c='c', st='t', sf='f', sm='flow', pj='p', cj='c')
    plt.show()
    ## Test 2.2
    h = plotAdjacency (adjacency, node_labels=label, node_color=color,\
        node_size=6, edge_width=2)
    plt.tight_layout()
    plt.subplots_adjust(left=0, bottom=0, right=1, top=1, wspace=0, hspace=0)
    plt.margins(x=0, y=0)
    plt.show()
    ## Test 2.3
    h = plotAdjacency (adjacency, node_labels=label, node_color=color,\
        node_positions=posxy, node_size=6, edge_width=2)
    plt.tight_layout()
    plt.subplots_adjust(left=0, bottom=0, right=1, top=1, wspace=0, hspace=0)
    plt.margins(x=0, y=0)
    plt.show()
    ## Test 2.4
    draw(adjacency)
    plt.tight_layout()
    plt.subplots_adjust(left=0, bottom=0, right=1, top=1, wspace=0, hspace=0)
    plt.margins(x=0, y=0)
    plt.show()
    ## Test 2.5
    kwargs = {}
    kwargs['draw_arrows'] = True
    kwargs['node_size'] = 6
    kwargs['edge_width'] = 2
    draw(adjacency, node_labels=label, node_color=color,\
        node_positions=posxy, **kwargs)
    plt.tight_layout()
    plt.subplots_adjust(left=0, bottom=0, right=1, top=1, wspace=0, hspace=0)
    plt.margins(x=0, y=0)
    plt.show()
    ## Test 3
    A = np.zeros([7,7])
    A[0,2] = 1
    A[0,3] = 1
    A[1,2] = 1
    A[2,3] = 1
    A[3,4] = 1
    A[1,4] = 1
    A[5,3] = 1
    A[3,6] = 1
    label = {0:1, 1:2, 2:3, 3:4, 4:5, 5:6, 6:7}
    draw(A, node_labels=label, draw_arrows=True, node_size=6)
    plt.tight_layout()
    plt.subplots_adjust(left=0, bottom=0, right=1, top=1, wspace=0, hspace=0)
    plt.margins(x=0, y=0)
    plt.show()
    ## Test 4
    import json
    jsonfile = "data/sistema4.json"
    fid = open(jsonfile, "r")
    ds = fid.readlines()
    fid.close()
    ds = json.loads("".join(ds))
    adjacency, label, color, posxy = makeAdjacency (ds,\
        st='t', sf='f', sm='m', pj='n', cj='n',\
        node_size=8, node_label_font_size=6, edge_width=2)
    plt.show()
    ## Test 5
    # jsonfile = "data/basic.json"
    # fid = open(jsonfile, "r")
    # ds = fid.readlines()
    # fid.close()
    # ds = json.loads("".join(ds))
    # plotUsingNetworkX (ds)
    ## Test 6
    # jsonfile = "data/sistema4.json"
    # fid = open(jsonfile, "r")
    # ds = fid.readlines()
    # fid.close()
    # ds = json.loads("".join(ds))
    # plotUsingNetworkX (ds, st='t', sf='f', sm='m', pj='n', cj='n')

