#!/usr/bin/env python3
# vim: set fileencoding=utf-8 fileformat=unix :
# -*- coding: utf-8 -*-
# vim: set ts=8 et sw=4 sts=4 sta :
#######
##
## calcSoundSpeed.py -- Routines to compute the sound speed and other
##                      thermodynamic properties of gas mixtures
##
## Author: Carlos Veiga Rodrigues <carlos.rodrigues@fe.up.pt>
## Copyright (C) 2018 Carlos Veiga Rodrigues <carlos.rodrigues@fe.up.pt>
## License: LGPLv3 - GNU Lesser General Public License, version 3
##          <http://www.gnu.org/licenses/lgpl-3.0.txt>
##
##   This program is free software: you can redistribute it and/or
##   modify it under the terms of the GNU Lesser General Public License
##   as published by the Free Software Foundation, either version 3
##   of the License, or (at your option) any later version.
##  
##   This program is distributed in the hope that it will be useful,
##   but WITHOUT ANY WARRANTY; without even the implied warranty of
##   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
##   GNU Lesser General Public License for more details.
## 
##  You should have received a copy of the GNU Lesser General Public License
##  along with this program. If not, see <https://www.gnu.org/licenses/>.
##
#######

from math import *
import numpy as np
import scipy as sp
from scipy.optimize import bisect, newton, brentq

eps = np.finfo(float).eps


## Constantes
R0 = 8314.41  # J/kmol/K
Mar = 28.9625  # kg/kmol


def pontoCriticoGarb (dgas):
    """
    Method to estimate the pseudocritical conditions (Tpc, Ppc) through
    the relative density of the gas.
    Garb (1978) Petrol Eng Int, 50:p56, "Property Evaluation with Hand-held
    Calculators, Part II – Moving Averages and Gas Deviation Factor"
    """
    # dgas = RHOgas / RHOar
    Tpc = (167 + 316.67*dgas) * 5/9.  # K
    Ppc = (702.5 - 50*dgas)*6894.76  # Pa
    return Tpc, Ppc


#######
##
## Algoritmo de calculo
##
#######
def ZDranchukPurvisRobinson (Tr, Pr, tol=np.sqrt(np.finfo(float).eps)):
    """
    Method to compute Standing & Katz Z compressibility factor through the
    Benedict-Webb-Rubin (BWR) state equation for real gases.
    Ref: Dranchuk Purvis Robinson (1973) Paper CIM 73-112
         Proc 24th Annu Tech Meeting Petro Soc CIM, Canada, May 8–12
    http://github.com/cran/zFactor/blob/master/R/Dranchuk-Purvis-Robinson.R
    """
    A1 =  0.31506237
    A2 = -1.04670990
    A3 = -0.57832729
    A4 =  0.53530771
    A5 = -0.61232032
    A6 = -0.10488813
    A7 =  0.68157001
    A8 =  0.68446549
    T1 = A1 + A2/Tr + A3/Tr**3
    T2 = A4 + A5/Tr
    T3 = A5*A6/Tr
    T4 = A7/Tr**3
    Z_RHOr = 0.27*Pr/Tr  # Z*RHOr = 0.27 * Pr / Tr
    RHOr = 0.27*Pr/Tr  # initial guess with Z=1
    fun = lambda RHOr: 1 + T1*RHOr + T2*RHOr**2 + T3*RHOr**5 \
        + T4*RHOr**2 * (1 + A8*RHOr**2)*np.exp(-A8*RHOr**2) \
        - Z_RHOr/RHOr
    dfun = lambda RHOr: T1 + 2*T2*RHOr + 5*T3*RHOr**4 \
        + 2*T4*RHOr*np.exp(-A8*RHOr**2)\
        * ((1 + 2*A8*RHOr**2) - A8*RHOr**2 * (1 + A8*RHOr**2)) \
        + Z_RHOr/RHOr**2
    RHOr = newton(fun, RHOr, fprime=dfun, tol=tol)
    Z = 0.27*Pr/Tr/RHOr
    return Z


def Cp0DranchukAbouKassem (dgas, T, Tpc, Pr=None, XCO2=0, XN2=0, XH2S=0,\
    Mar=28.9625, R0 = 8314.41, verbose=False):
    """
    Method to compute Cp0 (ideal) from dgas = RHOgas/RHOar = Mgas/Mar
    Dranchuk Abou-Kassem (1992) doi: 10.1002/cjce.5450700220
    """
    if verbose:
        if XCO2 > 0.18:
            print ("Warning: restrictions not met, XCO2 > 0.18, result will be afected")
        if XN2 > 0.25:
            print ("Warning: restrictions not met, XN2  > 0.25, result will be afected")
        if XH2S > 0.34:
            print ("Warning: restrictions not met, XH2S > 0.34, result will be afected")
        if T < 300 or T > 1500:
            print ("Warning: restrictions not met, T out-of-range (300 < T < 1500)")
    if T <= 900:  # 300 < T < 900 K
        B1  = 19.77350
        B2  = -7.206846
        B3  = 13.46951
        B4  = -3.82984
        B5  = 18.29800
        B6  = 12.76490
        B7  = -560.56
        B8  = -338.33
        B9  = -423.33
        B10 = -516.11
        B11 = -102.78
        B12 = 1687.8
    else:  # 900 < T < 1500 K
        B1  = 26.44520
        B2  = -0.2650925
        B3  = 48.33881
        B4  = -4.063599
        B5  = 19.92026
        B6  = -3.376940
        B7  = -790.56
        B8  = -4982.2
        B9  = 572.22
        B10 = -1890.6
        B11 = 139.44
        B12 = -8538.9
    Tc = Tpc \
        - B7*XCO2 - B8*XCO2**2 \
        - B9*XN2 - B10*XN2**2 \
        - B11*XH2S - B12*XH2S**2
    A = B1 + B2*dgas + B3*dgas**2
    B = B4 + B5*dgas + B6*dgas**2
    Cp0m = (A + B*T/Tpc) * 1e3  # J/kmol/K
    M = dgas*Mar  # kg/kmol
    Cp0 = Cp0m / M  # J/kg/K
    ## Heat capacity departure
    if Pr is None:
        return Cp0
    else:
        A1 =  0.31506237
        A2 = -1.04670990
        A3 = -0.57832729
        A4 =  0.53530771
        A5 = -0.61232032
        A6 = -0.10488813
        A7 =  0.68157001
        A8 =  0.68446549
        Tr = T / Tpc
        Z = ZDranchukPurvisRobinson (Tr, Pr)
        RHOr = 0.27*Pr/Z/Tr
        dCp = - 1 \
            - 6*A3/Tr**3*RHOr \
            - 6*A7/A8/Tr**3 \
            + (6*A7/A8/Tr**3 + 3*A7/Tr**3*RHOr**2)*np.exp(-A8*RHOr**2) \
            + (1 + (A1 - 2*A3/Tr**3)*RHOr + A4*RHOr**2  \
            - 2*A7/Tr**3*RHOr**2*(1 + A8/RHOr**2)*np.exp(-A8*RHOr**2))**2 \
            / (1. + 2*(A1 + A2/Tr + A3/Tr**3)*RHOr + 3*(A4 + A5/Tr)*RHOr**2 \
            + 6*A5*A6/Tr*RHOr**5 + A7/Tr**3*RHOr**2 \
            * (3 + 3*A8*RHOr**2 - 2*A8**2*RHOr**4)*np.exp(-A8*RHOr**2))
        dCp *= R0/M  # to have (Cp-Cp0) in J/kg/K
    return Cp0, dCp


#######
##
## Algoritmo de calculo
##
## Método para calculo dp Cp, Cv, csound
## Thomas, Hankinson, Phillips (1970) doi: 10.2118/2579-PA
## como a equacao de Benedict-Webb-Rubin (BWR), as constantes
## de Dranchuk Purvis Robinson (1973) vão ser utilizadas
##
#######
def calcCpCvCsound (dgas, Z, T, P, Tpc, Ppc, XCO2=0, XN2=0, XH2S=0):
    """
    Method to compute Cp, Cv and sound speed, described in:
    Thomas Hankinson Phillips (1970) doi: 10.2118/2579-PA
    This method makes use of the Benedict-Webb-Rubin (BWR) equation of state,
    thus the constants from Dranchuk Purvis Robinson (1973) are used instead.
    """
    def dPdT_Vcte (T, RHO, Tpc, Ppc, R, Zc=0.27):
        A1 =  0.31506237
        A2 = -1.04670990
        A3 = -0.57832729
        A4 =  0.53530771
        A5 = -0.61232032
        A6 = -0.10488813
        A7 =  0.68157001
        A8 =  0.68446549
        Tr = T/Tpc
        RHOpc = Ppc/Zc/R/Tpc
        RHOr = RHO/RHOpc
        dPdT = (Ppc*RHOr + (A1 - 2*A3/Tr**3)*RHOr**2 + A4*RHOr**3 \
            - 2*A7*(RHOr/Tr)**3 * (1 + A8*RHOr**2) * np.exp(-A8*RHOr**2) \
            ) / Zc / Tpc
        return dPdT
    def dPdV_Tcte (T, RHO, Tpc, Ppc, R, Zc=0.27):
        A1 =  0.31506237
        A2 = -1.04670990
        A3 = -0.57832729
        A4 =  0.53530771
        A5 = -0.61232032
        A6 = -0.10488813
        A7 =  0.68157001
        A8 =  0.68446549
        Tr = T/Tpc
        RHOpc = Ppc/Zc/R/Tpc
        RHOr = RHO/RHOpc
        dPdV = (-Ppc*Tr*RHOr**2 - 2*RHOr**3 * (A1*Tr + A2 + A3/Tr**2) \
            - 3*RHOr**4 * (A4*Tr + A5) - 6*RHOr**7*A5*A6 + A7/Tr**2 * ( \
            - 3*RHOr**4 * (1 + A8*RHOr**2)*np.exp(-A8*RHOr**2) \
            - 2*A8*RHOr**6 * np.exp(-A8*RHOr**2) \
            + 2*A8*RHOr**6 * (1 + A8*RHOr**2)*np.exp(-A8*RHOr**2)) \
            ) * RHOpc / Zc
        return dPdV
    Mgas = dgas * Mar
    Rgas = R0 / Mgas
    RHO = P/Z/Rgas/T
    Tr = T/Tpc
    dPdT_V = dPdT_Vcte (T, RHO, Tpc, Ppc, Rgas)
    dPdV_T = dPdV_Tcte (T, RHO, Tpc, Ppc, Rgas)
    CpmCv = - T * dPdT_V**2 / dPdV_T
    Cp0, dCp = Cp0DranchukAbouKassem (dgas, T, Tpc, Pr=P/Ppc, \
        XCO2=XCO2, XN2=XN2, XH2S=XH2S)
    Cv0 = Cp0 - Rgas
    Cp = dCp + Cp0
    Cv = Cp - CpmCv
    K0 = Cp0 / Cv0
    K = Cp / Cv
    ## dPdV_Scte = Cp/Cv * dPdV_Tcte
    dPdV_S = K * dPdV_T
    ## isentropic expansion coefficient
    n = - dPdV_S / P / RHO
    csound = sqrt(n*Z*Rgas*T)
    return csound, Cp, Cv, Cp0, Cv0


#######
##
## Algoritmo de calculo
##
## Metodos para calculo do Z
## in "Evaluating Gas Network Capacities" (2015) eds Koch et al, SIAM
## Originaly proposed in
## Králik, Stiegler, Vostrý, Záworka (1988) "Dynamic Modeling of
## Large-Scale Networks with Application to Gas Distribution", Vol 6.
## Studies in Automation and Control series, Elsevier
##
#######
def ZKralik (Tr, Pr):
    return 1 + 0.257*Pr - 0.533*Pr/Tr

def calcCpCvCsound_Kralik (dgas, Z, T, P, Tpc, Ppc, XCO2=0, XN2=0, XH2S=0):
    def ZKralik_alt (T, RHO, Tpc, Ppc, R):
        a = R * 0.257 / Ppc
        b = R * 0.533 * Tpc / Ppc
        return 1 / (1. + b*RHO - a*RHO*T)
    def dZdRHO (T, RHO, Tpc, Ppc, R):
        a = R * 0.257 / Ppc
        b = R * 0.533 * Tpc / Ppc
        return - (b - a*T) / (1. + b*RHO - a*RHO*T)**2
    def dZdT (T, RHO, Tpc, Ppc, R):
        a = R * 0.257 / Ppc
        b = R * 0.533 * Tpc / Ppc
        return a*RHO / (1. + b*RHO - a*RHO*T)**2
    def d2ZdT2 (T, RHO, Tpc, Ppc, R):
        a = R * 0.257 / Ppc
        b = R * 0.533 * Tpc / Ppc
        return 2*(a*RHO)**2 / (1. + b*RHO - a*RHO*T)**3
    Mgas = dgas * Mar
    Rgas = R0 / Mgas
    ## Nao encontrei outra forma para calcular Cp0
    Cp0 = Cp0DranchukAbouKassem (dgas, T, Tpc, XCO2=XCO2, XN2=XN2, XH2S=XH2S)
    Cv0 = Cp0 - Rgas
    if Cp0 < Rgas:
        print ("Warning: Cp0 < Rgas (Rgas = %g J/kg/K, Cp0 = %g J/kg/K)"\
            % (Rgas, Cp0))
    RHO = P/Z/Rgas/T
    ## \int_0_\rho \frac{2\,T}{\rho} \left(\frac{\partial Z}{\partial T}\right)_\rho d\rho
    int1, err = sp.integrate.quad(\
        lambda x: 2*T/x * dZdT (T, x, Tpc, Ppc, Rgas), 0, RHO)
    ## \int_0_\rho \frac{T^2}{\rho} \left(\frac{\partial^2 Z}{\partial T^2}\right)_\rho d\rho
    int2, err = sp.integrate.quad(\
        lambda x: T**2/x * d2ZdT2 (T, x, Tpc, Ppc, Rgas), 0, RHO)
    ## Calculo de K
    Cv = Cp0 - Rgas*(1. + int1 + int2)
    if Cv < 0:
        print ("Warning: Cv negative (Cv = %g J/kg/K)" % Cv)
    Cp = Cv + Rgas \
        * (Z + T * dZdT (T, RHO, Tpc, Ppc, Rgas))**2 \
        / (Z + RHO * dZdRHO (T, RHO, Tpc, Ppc, Rgas))
    if Cp < Cv:
        print ("Warning: Cp < Cv (Cp = %g J/kg/K, Cv = %g J/kg/K)" % (Cp, Cv))
    K = Cp / Cv
    if K < 0:
        print ("Warning: K negative (K = %g)" % K)
    csound2 = K * Rgas * T * (Z + RHO * dZdRHO (T, RHO, Tpc, Ppc, Rgas))
    return sqrt(abs(csound2)), Cp, Cv, Cp0, Cv0


#######
##
## Execucao
##
#######
if __name__ == '__main__':
    print(7*'=' + ' Calculo das propriedades de uma mistura gasosa ')
    #######
    ##
    ## Dados
    ##
    #######
    print("======  Exemplo 1, Ar")
    dgas = 1.
    Kar = 1.4
    Rar = R0/Mar
    XN2 = 0.78084
    XCO2 = 0.0004
    T, P, Tpc, Ppc = 300., 1e5, 132, 3.77E6
    csound = sqrt(Kar*Rar*T)
    print("real: T = %g K,  P = %g Pa,  c = %g m/s" % (T, P, csound))
    Cp0 = Rar*Kar/(Kar-1.)
    Cv0 = Cp0 - Rar
    Z = ZDranchukPurvisRobinson (T/Tpc, P/Ppc)
    csound, _, _, _, _ = calcCpCvCsound (dgas, Z, T, P, Tpc, Ppc,\
        XCO2=XCO2, XN2=XN2)
    print("calc: alg Thomas        = %g m/s" % csound)
    Z = ZKralik (T/Tpc, P/Ppc) 
    csound, _, _, _, _ = calcCpCvCsound_Kralik (dgas, Z, T, P, Tpc, Ppc,\
        XCO2=XCO2, XN2=XN2)
    print("calc: eq Kralik         = %g m/s" % csound)
    print("======  Exemplo 2, mistura real")
    print("para:  0.84992 CH4 + 0.15008 C2H6")
    XCH4, XC2H6 = 0.84992, 0.15008
    Mgas = XCH4*16.043 + XC2H6*30.070
    Tpc = XCH4*191 + XC2H6*305
    Ppc = XCH4*4.60E6 + XC2H6*4.88E6
    dgas = Mgas/Mar
    Rgas = R0/Mgas
    T, P, csound = 350., 10.457e6, 445.49
    print("real: T = %g K,  P = %g Pa,  c = %g m/s" % (T, P, csound))
    Cp0, dCp = Cp0DranchukAbouKassem (dgas, T, Tpc, P/Ppc)
    Cv0 = Cp0 - Rgas
    Cp = dCp + Cp0
    print("calc: sqrt(Cp0/Cv0*R*T) = %g m/s" % sqrt(Cp0/Cv0*Rgas*T))
    print("calc: sqrt(Cp/(Cp-Rgas)*R*T)   = %g m/s" % sqrt(Cp/(Cp-Rgas)*Rgas*T))
    Z = ZDranchukPurvisRobinson (T/Tpc, P/Ppc)
    csound, _, _, _, _ = calcCpCvCsound (dgas, Z, T, P, Tpc, Ppc)
    print("calc: alg Thomas        = %g m/s" % csound)
    Z = ZKralik (T/Tpc, P/Ppc) 
    csound, _, _, _, _ = calcCpCvCsound_Kralik (dgas, Z, T, P, Tpc, Ppc)
    print("calc: eq Kralik         = %g m/s" % csound)
    print("======  Exemplo 3.1, mistura real")
    ## Statoil Statvordgass: CH4, C2H6, C3H8, nC4, nC5, nC6, N2, CO2
    X = np.array([0.74348, 0.12005, 0.08251, 0.03026, 0.00575, 0.00230, 0.00537, 0.01028])
    M = np.array([16.043, 30.070, 44.097, 58.124, 72.151, 86.178, 28.016, 44.010])
    Tc = np.array([191, 305, 370, 425, 470, 507, 126, 304])
    Pc = np.array([4.60E6, 4.88E6, 4.25E6, 3.80E6, 3.37E6, 3.01E6, 3.40E6, 7.38E6])
    Mgas = sum(X*M)
    Tpc = sum(X*Tc)
    Ppc = sum(X*Pc)
    dgas = Mgas/Mar
    Rgas = R0/Mgas
    ## valores reais 1
    T, P, csound = 300., 1.863e6, 359.32
    print("real: T = %g K,  P = %g Pa,  c = %g m/s" % (T, P, csound))
    Cp0, dCp = Cp0DranchukAbouKassem (dgas, T, Tpc, P/Ppc, XN2=X[-2], XCO2=X[-1])
    Cv0 = Cp0 - Rgas
    Cp = dCp + Cp0
    print("calc: sqrt(Cp0/Cv0*R*T) = %g m/s" % sqrt(Cp0/Cv0*Rgas*T))
    print("calc: sqrt(Cp/(Cp-Rgas)*R*T)   = %g m/s" % sqrt(Cp/(Cp-Rgas)*Rgas*T))
    Z = ZDranchukPurvisRobinson (T/Tpc, P/Ppc)
    csound, _, _, _, _ = calcCpCvCsound (dgas, Z, T, P, Tpc, Ppc,\
        XCO2=X[-1], XN2=X[-2])
    print("calc: alg Thomas        = %g m/s" % csound)
    Z = ZKralik (T/Tpc, P/Ppc) 
    csound, _, _, _, _ = calcCpCvCsound_Kralik (dgas, Z, T, P, Tpc, Ppc,\
        XCO2=X[-1], XN2=X[-2])
    print("calc: eq Kralik         = %g m/s" % csound)
    ## valores reais 2
    print("======  Exemplo 3.2, mistura real")
    T, P, csound = 325., 5.710e6, 359.94
    print("real: T = %g K,  P = %g Pa,  c = %g m/s" % (T, P, csound))
    Z = ZDranchukPurvisRobinson (T/Tpc, P/Ppc)
    Cp0, dCp = Cp0DranchukAbouKassem (dgas, T, Tpc, P/Ppc, XN2=X[-2], XCO2=X[-1])
    Cv0 = Cp0 - Rgas
    Cp = dCp + Cp0
    print("calc: sqrt(Cp0/Cv0*R*T) = %g m/s" % sqrt(Cp0/Cv0*Rgas*T))
    print("calc: sqrt(Cp/(Cp-Rgas)*R*T)   = %g m/s" % sqrt(Cp/(Cp-Rgas)*Rgas*T))
    Z = ZDranchukPurvisRobinson (T/Tpc, P/Ppc)
    csound, _, _, _, _ = calcCpCvCsound (dgas, Z, T, P, Tpc, Ppc,\
        XCO2=X[-1], XN2=X[-2])
    print("calc: alg Thomas        = %g m/s" % csound)
    Z = ZKralik (T/Tpc, P/Ppc) 
    csound, _, _, _, _ = calcCpCvCsound_Kralik (dgas, Z, T, P, Tpc, Ppc,\
        XCO2=X[-1], XN2=X[-2])
    print("calc: eq Kralik         = %g m/s" % csound)
    ## valores reais 3
    print("======  Exemplo 3.3, mistura real")
    T, P, csound = 350., 10.436e6, 382.53
    print("real: T = %g K,  P = %g Pa,  c = %g m/s" % (T, P, csound))
    Z = ZDranchukPurvisRobinson (T/Tpc, P/Ppc)
    Cp0, dCp = Cp0DranchukAbouKassem (dgas, T, Tpc, P/Ppc, XN2=X[-2], XCO2=X[-1])
    Cv0 = Cp0 - Rgas
    Cp = dCp + Cp0
    print("calc: sqrt(Cp0/Cv0*R*T) = %g m/s" % sqrt(Cp0/Cv0*Rgas*T))
    print("calc: sqrt(Cp/(Cp-Rgas)*R*T)   = %g m/s" % sqrt(Cp/(Cp-Rgas)*Rgas*T))
    Z = ZDranchukPurvisRobinson (T/Tpc, P/Ppc)
    csound, _, _, _, _ = calcCpCvCsound (dgas, Z, T, P, Tpc, Ppc,\
        XCO2=X[-1], XN2=X[-2])
    print("calc: alg Thomas        = %g m/s" % csound)
    Z = ZKralik (T/Tpc, P/Ppc) 
    csound, _, _, _, _ = calcCpCvCsound_Kralik (dgas, Z, T, P, Tpc, Ppc,\
        XCO2=X[-1], XN2=X[-2])
    print("calc: eq Kralik         = %g m/s" % csound)
    ## valores reais 4
    print("======  Exemplo 3.4, mistura real")
    T, P, csound = 350., 0.641e6, 396.46
    print("real: T = %g K,  P = %g Pa,  c = %g m/s" % (T, P, csound))
    Z = ZDranchukPurvisRobinson (T/Tpc, P/Ppc)
    Cp0, dCp = Cp0DranchukAbouKassem (dgas, T, Tpc, P/Ppc, XN2=X[-2], XCO2=X[-1])
    Cv0 = Cp0 - Rgas
    Cp = dCp + Cp0
    print("calc: sqrt(Cp0/Cv0*R*T) = %g m/s" % sqrt(Cp0/Cv0*Rgas*T))
    print("calc: sqrt(Cp/(Cp-Rgas)*R*T)   = %g m/s" % sqrt(Cp/(Cp-Rgas)*Rgas*T))
    Z = ZDranchukPurvisRobinson (T/Tpc, P/Ppc)
    csound, _, _, _, _ = calcCpCvCsound (dgas, Z, T, P, Tpc, Ppc,\
        XCO2=X[-1], XN2=X[-2])
    print("calc: alg Thomas        = %g m/s" % csound)
    Z = ZKralik (T/Tpc, P/Ppc) 
    csound, _, _, _, _ = calcCpCvCsound_Kralik (dgas, Z, T, P, Tpc, Ppc,\
        XCO2=X[-1], XN2=X[-2])
    print("calc: eq Kralik         = %g m/s" % csound)

